#!/bin/bash
#SBATCH --job-name=QR_PyTool
#SBATCH --partition=gpu
#SBATCH --ntasks=1
#SBATCH --gpus-per-node=2
#SBATCH --cpus-per-task=6
#SBATCH --mem=16G
#SBATCH --time=20-00:00:00
## SBATCH --nodelist=eris
#SBATCH --exclude=alpos

hostname
echo $CUDA_VISIBLE_DEVICES
nvidia-smi

embeddings='""' # let void or pass w2vCNN or w2vTRSFMR as mapped arg"

for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done

if [ -z $load_model ]; then

   result_folder="exp/pytorch_resnet_block256_ecapatdnn_good/"$architecture"_"$task
   mkdir -p $result_folder

   if [ -z $acoustic_model ]; then 


      
      # --------------------------- Example usage Without acoustic model-----------------------------
      # sbatch run_train_test.sh batch_size=128 num_epoch=10 num_iterations=75 lr=0.01 architecture=tdnn task=sentiment
      # sbatch run_train_test.sh batch_size=128 num_epoch=3 num_iterations=75 lr=0.01 architecture=mlp task=emotion

      ## --------------------------- Example usage With Wav2Vec-----------------------------
      # sbatch run_train_test.sh batch_size=128 num_epoch=3 num_iterations=75 lr=0.001 architecture=tdnn task=emotion embeddings=w2vTRSFMR
      # sbatch run_train_test.sh batch_size=128 num_epoch=3 num_iterations=75 lr=0.001 architecture=tdnn task=emotion embeddings=w2vCNN
      
      mkdir -p $result_folder'/baseline'

      python3 train_test.py\
       --architecture $architecture\
       --task $task\
       --embeddings $embeddings\
       --batch_size $batch_size\
       --lr $lr\
       --weight_decay 0.001\
       --num_epochs $num_epoch\
       --num_iterations $num_iterations\
       --max_seq_len 600\
       --model_dir $result_folder'/baseline'\
       --kaldi_pybind_path /users/qraymondaud/kaldi/src/pybind\
       --feature_size 40\

   else

      # --------------------------- Example usage With acoustic model-----------------------------
      # Short Train Run : sbatch run_train_test.sh batch_size=128 num_epoch=3 num_iterations=75 lr=0.0001 architecture=tdnn task=sentiment acoustic_model=final1  
      # ...

      result_folder="exp/pytorch_resnet_block256_ecapatdnn_good/"$architecture"_"$task

      mkdir -p $result_folder'/'$acoustic_model

      python3 train_test.py\
       --architecture $architecture\
       --task $task\
       --embeddings $embeddings\
       --batch_size $batch_size\
       --lr $lr\
       --weight_decay 0.001\
       --num_epochs $num_epoch\
       --num_iterations $num_iterations\
       --max_seq_len 600\
       --model_dir $result_folder'/'$acoustic_model\
       --kaldi_pybind_path /users/qraymondaud/kaldi/src/pybind\
       --feature_size 1536\
       --acoustic_model '/data/coros1/qraymondaud/acoustic_models/'$acoustic_model'.raw'\

   fi

else 

   mkdir -p $result_folder'/'$acoustic_model

   result_folder="exp/pytorch_resnet_block256_ecapatdnn_good"

   python3 train_test.py\
    --architecture $architecture\
    --embeddings $embeddings\
    --task $task\
    --lr $lr\
    --weight_decay 0.001\
    --model_dir $result_folder'/test_models'\
    --kaldi_pybind_path /users/qraymondaud/kaldi/src/pybind\
    --feature_size 1536\
    --acoustic_model '/data/coros1/qraymondaud/acoustic_models/'$acoustic_model'.raw'\
    --load_model $load_model\
    --mode 'eval'
fi
