# Quentin RAYMONDAUD
# pip3 install numpy seaborn 
# example usage : python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/
#                 python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/ cum
#                 python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/ each

import os, sys
import numpy as np
import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt


PRECISION = 4

labels = [
    ["anger",
    "disgust",
    "fear",
    "joy",
    "neutral",
    "sadness",
    "surprise"],

    ["negative",
    "neutral",
    "positive"]
]

if len(sys.argv) >= 2:
    setup = False
    path = sys.argv[1]
    allConf = sys.argv[2] if (len(sys.argv) >= 3) else "none"
    pathss = []
    for root, dirs, files in os.walk(path):
        for filename in files:
            if "final"  in filename or "MFCC" in filename:
                paths = os.path.join(root, filename)
                with open(paths, "r") as file:
                    lines = file.readlines()
                    pathss.append(paths)
                    strMatrix = ""
                    strResult = ""

                    cnt = 0 
                    
                    beginingIndex = 0
                    endIndex = 0
                    good = False

                    for line in lines:
                        if "on TEST" in line:
                            print(line)
                            good = True
                        elif good and "Confusion" in line:
                            beginingIndex = cnt+1
                        elif "F-Measure" in line and beginingIndex != 0:
                            endIndex = cnt
                            break
                        cnt+=1

                    if not setup:
                        if ( endIndex - beginingIndex > 3):
                            labels=labels[0]
                        else :
                            labels=labels[1]
                        setup = True

                    for line in lines[beginingIndex:endIndex]:
                        line = (
                            line.replace("'", "")
                            .replace("\n", "")
                            .replace("]", ";")
                            .replace("[", "")
                            .replace(",", "")
                            .replace(";;", "")
                        )
                        strMatrix += line

                    matrix = np.matrix(strMatrix)

                    if allConf == "all" or allConf == "each":
                        df_cm = pd.DataFrame(matrix, index=labels, columns=labels)
                        plt.figure(figsize=(10, 7))
                        plt.title(paths)
                        sn.heatmap(df_cm, annot=True)

                    matrixZ = np.array(matrix)

                    diag = np.diag(matrixZ)
                    p = diag / np.sum(matrixZ, axis=0)
                    r = diag / np.sum(matrixZ, axis=1)
                    f = (2*p*r)/(p+r)

                    print(filename + " - Precision : " + str(p))
                    print(filename + " - Recall : " + str(r))
                    print(filename + " - F-Measure : " + str(f))

                    a = np.sum(diag) / matrixZ.sum()  
                    print(filename + " - Accuracy : " + str(a))

    if (allConf != "none"):
        plt.show()