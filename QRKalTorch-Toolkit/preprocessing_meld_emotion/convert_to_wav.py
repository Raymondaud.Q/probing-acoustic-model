#Quentin RAYMONDAUD 

import csv, sys,  os

def mkdir(path):
  try:
    os.mkdir(path)
  except OSError as error:
    print(error)

CSV_paths = ["./train_sent_emo.csv", "./dev_sent_emo.csv", "./test_sent_emo.csv"]
CORPUSES = ["TRAIN", "DEV", "TEST"]
FOLDER = "WAV"



mkdir(FOLDER)

for i in range(len(CSV_paths)):

  mkdir(FOLDER+"/"+CORPUSES[i]+"_WAV")

  # Opening the CSV file
  with open(CSV_paths[i], mode ='r') as file:
    header_skipped = False
    # Reading the CSV file
    csvFile = csv.reader(file)

    # Displaying CSV file content 
    for lines in csvFile:

      if header_skipped :

        print("Speaker => " + lines[2].replace(' ',''))
        print("EMOTION => " + lines[3])
        print("SENTIMENT => " + lines[4])
        print("DIAL ID => "+lines[5])    
        print("UTT ID => "+lines[6])
        # https://stackoverflow.com/questions/61491258/convert-m4a-to-wav-file-containing-signed-16-bit-pcm-samples-in-ffmpeg
        # You can add -c:a pcm_s16le output option if you prefer, but that's the default 16bit encoder for WAV so it can be omitted.
        os.system("ffmpeg -i ./" + CORPUSES[i]+ "_split" + "/dia" + lines[5] + "_utt" + lines[6] + ".mp4 -ac 1 -ar 16000 -f wav " + FOLDER + "/" + CORPUSES[i] + "_WAV" + "/" + lines[2].replace(' ','') + "_" + lines[3] + "_" + lines[4] + "_dia" + lines[5] + "_utt" + lines[6] + ".wav")
      
      else :

        header_skipped = True
