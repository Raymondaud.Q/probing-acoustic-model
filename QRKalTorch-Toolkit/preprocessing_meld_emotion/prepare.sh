#! /bin/bash


#SBATCH --job-name=MELD_PREPRO
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=cpuonly
#SBATCH --mem=5G
#SBATCH --time=2-00:00:00

python3 convert_to_wav.py