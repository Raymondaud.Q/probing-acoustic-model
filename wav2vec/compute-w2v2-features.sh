#!/bin/bash
#SBATCH --job-name=embedw2v
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=gpu
#SBATCH --gpus-per-node=1
#SBATCH --constraint='GPURAM_Max_16GB'
#SBATCH --mem=32G
#SBATCH --time=7-0:00:00

. ./path.sh

#PYTORCH_NO_CUDA_MEMORY_CACHING=1


for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done


python3 ./w2v2-feat-extractor.py \
    --input=$input\
    --output=$output\
    --model-cache-dir=.cache/transformers/\
    $@ 


#python3 ./w2v2-feat-extractor.py \
#    --input="/data/coros1/qraymondaud/MELD/meld-mfcc-hires-2/TEST_WAV"\
#    --output="/data/coros1/qraymondaud/MELD/wav2vec_features"\
#    --model-cache-dir=.cache/transformers/ \
#    $@ # --view-output

# ./extract_wav2vec.py \
#     data/models/wav2vec2-FR-7K-large/checkpoint_best.pt \
#     data/train_sp/ \
#     data/train_sp_w2v/
