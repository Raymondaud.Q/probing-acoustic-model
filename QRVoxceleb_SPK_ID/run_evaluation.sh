dir=$1
acoustic_model=$2
feat_size=$3

. path.sh

local/pytorch/extract_xvectors.sh --cmd "slurm.pl --gpu 1 --config conf/slurm_gpu.conf" --nj 40  ${dir} data/voxceleb1/ ${dir}/voxceleb1/ ${acoustic_model} ${feat_size}

echo -e "\n**** VoxCeleb1-E Cleaned ****"

ivector-compute-dot-products 'cat data/voxceleb1/voxceleb1_e_cleaned.trials | cut -f1,2 -d" " |' "ark:ivector-normalize-length scp:${dir}/voxceleb1/xvector.scp ark:- |" "ark:ivector-normalize-length scp:${dir}/voxceleb1/xvector.scp ark:- |" ${dir}/voxceleb1/scores.txt  2> /dev/null

eer=`paste data/voxceleb1/voxceleb1_e_cleaned.trials ${dir}/voxceleb1/scores.txt | awk '{print $6, $3}' | compute-eer - 2> /dev/null`
mindcf1=`sid/compute_min_dcf.py --p-target 0.01 ${dir}/voxceleb1/scores.txt data/voxceleb1/voxceleb1_e_cleaned.trials 2> /dev/null`
mindcf2=`sid/compute_min_dcf.py --p-target 0.001 ${dir}/voxceleb1/scores.txt data/voxceleb1/voxceleb1_e_cleaned.trials 2> /dev/null`
echo "EER: $eer%"
echo "minDCF(p-target=0.01): $mindcf1"
echo "minDCF(p-target=0.001): $mindcf2"

echo -e "\n**** VoxCeleb1-H Cleaned ****"

ivector-compute-dot-products 'cat data/voxceleb1/voxceleb1_h_cleaned.trials | cut -f1,2 -d" " |' "ark:ivector-normalize-length scp:${dir}/voxceleb1/xvector.scp ark:- |" "ark:ivector-normalize-length scp:${dir}/voxceleb1/xvector.scp ark:- |" ${dir}/voxceleb1/scores.txt  2> /dev/null

eer=`paste data/voxceleb1/voxceleb1_h_cleaned.trials ${dir}/voxceleb1/scores.txt | awk '{print $6, $3}' | compute-eer - 2> /dev/null`
mindcf1=`sid/compute_min_dcf.py --p-target 0.01 ${dir}/voxceleb1/scores.txt data/voxceleb1/voxceleb1_h_cleaned.trials 2> /dev/null`
mindcf2=`sid/compute_min_dcf.py --p-target 0.001 ${dir}/voxceleb1/scores.txt data/voxceleb1/voxceleb1_h_cleaned.trials 2> /dev/null`
echo "EER: $eer%"
echo "minDCF(p-target=0.01): $mindcf1"
echo "minDCF(p-target=0.001): $mindcf2"



