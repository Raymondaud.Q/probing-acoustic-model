#!/usr/bin/env ruby

require "rubygems"

def random_target(x, ar, t)

  s = Array.new
  ar.each do |e|
    if x != e
      if t[x] == t[e]
        s.push( e )
      end
    end
  end
  number = rand(0..s.size-1)
  return s[number]

end



def random_nontarget(x, ar, t)
=begin
  s = Array.new
  ar.each do |e|
    if x != e
      if t[x] != t[e]
        s.push( e )
      end
    end
  end
=end

  s = x

  while t[x] == t[s]
    number = rand(0..ar.size-1)
    s = ar[number]
  end

  return s

end



def get_random(ar)
    x = rand(0..ar.size-1)
    y = rand(0..ar.size-1)
    return ar[x], ar[y]
end


def get_random_same(ar, h)
    x = rand(0..ar.size-1)
    t = h[ ar[x].split("#")[0] ]
    y = rand(0..t.size-1)
    return ar[x], t[y]
end


def launch(metainfo)


  target = 25000
  nontarget = 25000

  gender = Hash.new

  f = File.open(metainfo)
  f.each do |line|
    line.chomp!
    line = line.split("|")
    gender[ line[0].strip ] = line[1].strip.downcase
  end
  f.close



  ar = Array.new
  t = Hash.new
  classe = Hash.new

  $stdin.each do |line|
    line.chomp!
    ar.push( line )
    t[ line ] = line.split("#")[0]

    classe[ line.split("#")[0] ] ||= Array.new
    classe[ line.split("#")[0] ].push( line )
  end

  view = Hash.new(false)
  done = true
  counter = 0

  while done == true
    x, y = get_random(ar)

    if gender[ t[x] ] == "m"

      if gender[ t[x] ] == gender[ t[y] ]
        if x != y
          if view["#{x} #{y}"] == false
            if t[ x ] != t[ y ]
              puts "#{x} #{y} nontarget"
              view["#{x} #{y}"] = true
              view["#{y} #{x}"] = true
              counter += 1
            end
          end
        end
      end
    end

    done = false if counter == nontarget
  end



  view = Hash.new(false)
  done = true
  counter = 0

  while done == true
    x, y = get_random(ar)

    if gender[ t[x] ] == "f"
      if gender[ t[x] ] == gender[ t[y] ]
        if x != y
          if view["#{x} #{y}"] == false
            if t[ x ] != t[ y ]
              puts "#{x} #{y} nontarget"
              view["#{x} #{y}"] = true
              view["#{y} #{x}"] = true
              counter += 1
            end
          end
        end
      end
    end
    done = false if counter == nontarget
  end

  view = Hash.new(false)
  done = true
  counter = 0

  while done == true
    x, y = get_random_same(ar, classe)
    if gender[ t[x] ] == "m"
      if x != y
        if view["#{x} #{y}"] == false
          if t[ x ] == t[ y ]
            puts "#{x} #{y} target"
            view["#{x} #{y}"] = true
            view["#{y} #{x}"] = true
            counter += 1
          end
        end
      end
    end
    done = false if counter == target
  end

  view = Hash.new(false)
  done = true
  counter = 0

  while done == true
    x, y = get_random_same(ar, classe)
    if gender[ t[x] ] == "f"
      if x != y
        if view["#{x} #{y}"] == false
          if t[ x ] == t[ y ]
            puts "#{x} #{y} target"
            view["#{x} #{y}"] = true
            view["#{y} #{x}"] = true
            counter += 1
          end
        end
      end
    end
    done = false if counter == target
  end





end


def errarg
    puts "Usage : ./programme.rb"
    puts "Mickael Rouvier <mickael.rouvier@gmail.com>"
end


if ARGV.size == 1
    launch(ARGV[0])
else
    errarg
end

