#!/bin/bash

. path.sh

train_cmd="slurm.pl --mem 4G --config conf/slurm.conf"

for language in mls_french mls_german mls_dutch mls_polish mls_portuguese mls_spanish mls_italian
do

rm -rf data_fbank60/$language
mkdir data_fbank60/$language

find /local_disk/clytie/rouvier/multilingual_librispeech/${language}/{test,train,dev} -name "*.flac" | ruby -e 'STDIN.each do |line|  show=File.basename(line.chomp, ".flac");  speaker=show.split("_")[0]; livre=show.split("_")[1]; utterance=show.split("_")[2]; puts "#{ARGV[0]}\##{speaker}\##{livre}-#{utterance} sox #{line.chomp} -t wav - |" end' ${language} > data_fbank60/${language}/wav.scp

find /local_disk/clytie/rouvier/multilingual_librispeech/${language}/{test,train,dev} -name "*.flac" | ruby -e 'STDIN.each do |line|  show=File.basename(line.chomp, ".flac");  speaker=show.split("_")[0]; livre=show.split("_")[1]; utterance=show.split("_")[2]; puts "#{ARGV[0]}\##{speaker}\##{livre}-#{utterance} #{ARGV[0]}\##{speaker}" end' ${language} > data_fbank60/${language}/utt2spk

utils/utt2spk_to_spk2utt.pl data_fbank60/${language}/utt2spk > data_fbank60/${language}/spk2utt

utils/fix_data_dir.sh data_fbank60/${language}

steps/make_fbank.sh --cmd "$train_cmd" --write-utt2num-frames true --fbank-config conf/fbank60.conf --nj 10 data_fbank60/${language}/ exp/make_fbank60/${language} fbank60/${language}

sid/compute_vad_decision.sh --nj 10 --cmd "run.pl" data_fbank60/${language}/ exp/make_vad_fbank60/${language}/ fbank60/${language}/

utils/fix_data_dir.sh data_fbank60/${language}


#mv data_fbank60/${language}/vad.scp data_fbank60/${language}/vad.old.scp

#copy-vector scp:data_fbank60/${language}/vad.old.scp ark,t:- | ruby local/reduce_vad.rb | copy-vector ark,t:- ark,scp:data_fbank60/${language}/vad.ark,data_fbank60/${language}/vad.scp 

#cat /local_disk/clytie/rouvier/multilingual_librispeech/${language}/metainfo.txt | cut -f1,2 -d"|" > t1

#cat data_fbank60/${language}/utt2spk | cut -f1 -d" " | ruby local/mix.rb t1 > data_fbank60/${language}/trials


#utils/fix_data_dir.sh data_fbank60/${language}


done


