#!/bin/bash
#SBATCH --job-name=Train_xvector_v10ter
#SBATCH --partition=gpu
#SBATCH --ntasks=1
#SBATCH --gpus-per-node=4
#SBATCH --cpus-per-task=6
#SBATCH --mem=16G
#SBATCH --time=20-00:00:00


hostname
echo $CUDA_VISIBLE_DEVICES
nvidia-smi


#python3 local/pytorch/train_ecapatdnn_good.py --data data/voxceleb2_train/ --num_iterations 130000 --model_dir exp/pytorch_resnet_block256_ecapatdnn_good/ --batch_size 192 --max_seq_len 300 --load_model 130000

python3 local/pytorch/train_ecapatdnn_good.py\
 --kaldi_pybind_path /users/qraymondaud/kaldi/src/pybind\
 --data data/voxceleb2_train/\
 --num_iterations 130000\
 --model_dir exp/pytorch_resnet_block256_ecapatdnn_good/\
 --batch_size 192\
 --max_seq_len 300\
 --feature_size $2\
 --acoustic_model /data/coros1/qraymondaud/acoustic_models/$1.raw

