import numpy as np
import pydub
import sys
import os
import random
import speechbrain
import torch

from speechbrain.processing.speech_augmentation import SpeedPerturb, DropFreq, DropChunk, AddBabble, AddNoise, AddReverb
from speechbrain.dataio.dataio import read_audio, write_audio

print(len(sys.argv))

if len(sys.argv) == 3:
    rnd = random.randint(0,2)
    vitesse = [95,100,105]
    print(vitesse[rnd])

    a = pydub.AudioSegment.from_file(sys.argv[1])

    fl = np.float32(np.array(a.get_array_of_samples())) / 2**15

    waveforms = torch.from_numpy(fl).unsqueeze(0)
    length = torch.ones(1)

    speed_perturb = SpeedPerturb(perturb_prob=1.0, orig_freq=16000, speeds=[vitesse[rnd]])
    drop_freq = DropFreq(drop_prob=1.0, drop_count_low=0, drop_count_high=3)
    drop_chunk = DropChunk(drop_prob=1.0, drop_count_low=0, drop_count_high=5, drop_length_low=1000, drop_length_high=2000, noise_factor=0)

    waveforms = speed_perturb(waveforms)
    waveforms = drop_freq(waveforms)
    waveforms = drop_chunk(waveforms, length)

    write_audio(sys.argv[2], waveforms.squeeze(0), 16000)

