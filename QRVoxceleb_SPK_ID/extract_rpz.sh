#!/usr/bin/env bash

. ./cmd.sh
. ./path.sh

kaldi=/data/voxceleb/
models=/data/acoustic_models/
respath=/data/voxceleb/extract/

mkdir -p ${respath}/

for model in `ls ${models}/*.raw`
do

  for fold in voxceleb1 voxceleb2_train_cmvn; do
    utils/fix_data_dir.sh ${kaldi}/${fold}
    modelName=`basename ${model} .raw`
    mkdir -p ${respath}/${modelName}
    mkdir -p ${respath}/${modelName}/${fold}
    nnet3-compute --use-gpu=no $model scp:${kaldi}/${fold}/feats.scp ark,scp:${respath}/${modelName}/${fold}/feats.ark,${respath}/${modelName}/${fold}/feats.scp
    cp ${kaldi}/${fold}/utt2spk ${respath}/${modelName}/${fold}/utt2spk
    utils/fix_data_dir.sh ${respath}/${modelName}/${fold}
  done

done

# copy-feats scp:/data/UrbanSound8K/kaldi_extract/final1/fold1/feats.scp ark,t:- | more

