#!/usr/bin/env bash
# Copyright   2017   Johns Hopkins University (Author: Daniel Garcia-Romero)
#             2017   Johns Hopkins University (Author: Daniel Povey)
#        2017-2018   David Snyder
#             2018   Ewald Enzinger
# Apache 2.0.
#
# See ../README.txt for more info on data required.
# Results (mostly equal error-rates) are inline in comments below.

. ./cmd.sh
. ./path.sh
set -e
mkdir -p steps
mfccdir=`pwd`/mfcc
vaddir=`pwd`/mfcc
stage=0

if [ $stage -le 1 ]; then
  steps/make_mfcc.sh --write-utt2num-frames true --mfcc-config conf/mfcc_hires.conf --nj 40 --cmd "$train_cmd" data/voxceleb2_train/ exp/make_mfcc $mfccdir
  sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" /data/voxceleb/voxceleb2_train/ exp/make_vad $mfccdir
  utils/fix_data_dir.sh data/voxceleb2_train

  steps/make_mfcc.sh --write-utt2num-frames true --mfcc-config conf/mfcc_hires.conf --nj 40 --cmd "$train_cmd" data/voxceleb1/ exp/make_mfcc $mfccdir
  sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" /data/voxceleb/voxceleb1/ exp/make_vad $mfccdir
  utils/fix_data_dir.sh data/voxceleb1
fi



