
# Abstract 

Deep learning architectures have made significant progress
in terms of performance in many research areas. The automatic speech recognition (ASR) field has thus benefited
from these scientific and technological advances, particularly
for acoustic modeling, now integrating deep neural network
architectures. However, these performance gains have translated 
into increased complexity regarding the information
learned and conveyed through these “black-box” architectures. 
Following many researches in neural networks interpretability, 
we propose in this article a protocol that aims to
determine which and where information is located in an ASR
acoustic model (AM). To do so, we propose to evaluate AM
performance on a determined set of tasks using intermediate
representations (here, at different layer levels). Regarding
the performance variation and targeted tasks, we can emit
hypothesis about which information is enhanced or perturbed
at different architecture steps. Experiments are performed on
both speaker verification, acoustic environment classification,
gender classification, tempo-distortion detection systems and
speech sentiment/emotion identification. Analysis showed
that neural-based AMs hold heterogeneous information that
seems surprisingly uncorrelated with phoneme recognition,
such as emotion, sentiment or speaker identity. The low-level
hidden layers globally appears useful for the structuring of
information while the upper ones would tend to delete useless
information for phoneme recognition.

# Probing Acoustic Models 

This repository contains source-code and informations related to ICASSP 2023 paper "**PROBING THE INFORMATION ENCODED IN NEURAL-BASED ACOUSTIC MODELS OF AUTOMATIC SPEECH RECOGNITION SYSTEMS**"

* M.Rouvier - LIA, 
* R.Dufour - LS2N, 
* Q.Raymondaud - LIA

In order to understand which informations are encoded in the Kaldi ASR system, we extracted embeddings of several datasets for different tasks from different layers of the Acoustic Model.

Baseline results have been calculated using Kaldi HiRes MFCC features. We compared them to the results obtained with the extracted embeddings once the ECAPA-TDNN trained.

We used Voxeceleb, UrbanSound8K and MELD datasets for making Speaker Gender, Acoustic Environments, Speech Sentiments, Speech Emotions, Speaker Verification and Speaking Rate classification with an ECAPA-TDNN.


![schema](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/-/raw/master/Documents/Kaldi_Acoustic_Into_Voxceleb-ECAPA-TDNN.png)

# Source & Documentation

* [SpeechBrain-Kaldi : UrbanSound8K Acoustic Environments ](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/tree/master/SpeechBrain-UrbanSound-Recipe)
* [Kaldi-PyTorch-Wav2Vec ECAPA-TDNN - Toolkit for MELD Emotions, Sentiments & Voxceleb Speaking Rate](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/tree/master/QRKalTorch-Toolkit)
* [Kaldi-PyTorch ECAPA-TDNN - Voxceleb Speaker Verification Recipe (X-Vector)](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/tree/master/QRVoxceleb_SPK_ID)
* [Wav2Vec Embeddings Toolkit for MELD, UrbanSound8K & Voxceleb](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/tree/master/wav2vec)


