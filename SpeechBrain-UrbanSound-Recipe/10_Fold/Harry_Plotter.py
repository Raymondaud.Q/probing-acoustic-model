# Quentin RAYMONDAUD
# pip3 install numpy seaborn 
# example usage : python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/
#                 python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/ cum
#                 python3 Harry_Plotter.py ./PIPELINE_KADLI_SPEECHBRAIN/final1/ each

import os, sys
import numpy as np
import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt

labels = [
    'Aboiement',
    'Bruit d\'enfant',
    'Ventilation',
    'Musique de rue',
    'Coup de feu',
    'Sirène',
    'Moteur au ralenti',
    'Marteau Piqueur',
    'Perçage',
    'Klaxon de voiture'
]

PRECISION = 4

labels = [
    "dog_bark",
    "children_playing",
    "air_conditioner",
    "street_music",
    "gun_shot",
    "siren",
    "engine_idling",
    "jackhammer",
    "drilling",
    "car_horn",
]

if len(sys.argv) >= 2:
    path = sys.argv[1]
    allConf = sys.argv[2] if (len(sys.argv) >= 3) else "none"
    File = "train_log.txt"
    sumTrix = np.zeros((10, 10), dtype=int)

    precision = []
    fmeasure = []
    recall = []
    accuracy = []

    mean_p_per_label = []
    mean_r_per_label = []
    mean_f_per_label = []
    mean_a_per_fold = []
    pathss = []
    for root, dirs, files in os.walk(path):
        if File in files:
            paths = os.path.join(root, File)
            with open(paths, "r") as file:
                lines = file.readlines()
                pathss.append(paths)
                strMatrix = ""
                strResult = ""
                for line in lines[38:48]:
                    line = (
                        line.replace("'", "")
                        .replace("\n", "")
                        .replace("]", ";")
                        .replace("[", "")
                        .replace(",", "")
                        .replace(";;", "")
                    )
                    strMatrix += line

                matrix = np.matrix(strMatrix)
                sumTrix = sumTrix + matrix

                if allConf == "all" or allConf == "each":
                    df_cm = pd.DataFrame(matrix, index=labels, columns=labels)
                    plt.figure(figsize=(10, 7))
                    plt.title(paths)
                    sn.heatmap(df_cm, annot=True)

                matrixZ = np.array(matrix)
                diag = np.diag(matrixZ)
                p = diag / np.sum(matrixZ, axis=0)
                r = diag / np.sum(matrixZ, axis=1)
                f = (2*p*r)/(p+r)

                precision.append(p)
                recall.append(r)
                fmeasure.append(f)

                a = np.sum(diag) / matrixZ.sum()  
                accuracy.append(a)

    for fold in ['_1/', '_2/', '_3/', '_4/', '_5/', '_6/', '_7/', '_8/', '_9/', '_10/']:
        for cnt in range(10):
            if fold in pathss[cnt].split("/")[3]+"/":
                info = "\n-- Metrics --" + ' '.join(pathss[cnt].split("/")[0:3])
                print("_"*len(info))
                print(info)
                print("_"*len(info))
                for cnt2 in range(10):
                    line =  str(round(precision[cnt][cnt2], PRECISION))
                    toPrint = labels[cnt2] + " Precision = "
                    print(toPrint + line.rjust(60 - len(toPrint)))

                    line = str(round(recall[cnt][cnt2], PRECISION))
                    toPrint = labels[cnt2] + " Recall = "
                    print(toPrint + line.rjust(60 - len(toPrint)))

                    line = str(round(fmeasure[cnt][cnt2], PRECISION))
                    toPrint = labels[cnt2] + " F-measure = "
                    print(toPrint + line.rjust(60 - len(toPrint)) +"\n\n")

                val_p = round(np.mean(precision[cnt]), PRECISION)
                toPrint = "Mean Precision"
                print(toPrint + str(val_p).rjust(60 - len(toPrint)))

                val_r = round(np.mean(recall[cnt]), PRECISION)
                toPrint = "Mean Recall"
                print(toPrint + str(val_r).rjust(60 - len(toPrint)))

                val_f = round(np.mean(fmeasure[cnt]), PRECISION)
                toPrint = "Mean F-Measure"
                print(toPrint + str(val_f).rjust(60 - len(toPrint)))

                val_a = round(accuracy[cnt], PRECISION)
                toPrint = "Accuracy"
                print(toPrint + str(val_a).rjust(60 - len(toPrint)))

                mean_p_per_label.append(val_p)
                mean_r_per_label.append(val_r)
                mean_f_per_label.append(val_f)
                mean_a_per_fold.append(val_a)

    print("\n\nOverall mean Precision " + str( round( np.sum(mean_p_per_label)/len(labels), PRECISION)))
    print("\n\nOverall mean Recall "    + str( round( np.sum(mean_r_per_label)/len(labels), PRECISION)))
    print("\n\nOverall mean F-Measure " + str( round( np.sum(mean_f_per_label)/len(labels), PRECISION)))
    print("\n\nOverall mean Accuracy " + str( round( np.sum(mean_a_per_fold)/len(labels), PRECISION)))

    if allConf == "all" or allConf == "cum":
        print("\n\nSum of every confusion matrix of current folder")
        df_cm = pd.DataFrame(sumTrix, index=labels, columns=labels)
        plt.figure(figsize=(10, 7))
        sn.heatmap(df_cm, annot=True)
        plt.title(path)

    if allConf != 'none':
        plt.show()
else:
    print(
        "ARGUMENT[1] Missing : Expected Path to 10-FOLD X VALIDATION result dir"
        + " - Example usages underneath : \n"
        + "\t\tpython3 Harry_Plotter.py ./path_to_10_FOLD_X-valid_results/\n"
        + "\t\tpython3 Harry_Plotter.py ./path_to_10_FOLD_X-valid_results/ cum\n"
        + "\t\tpython3 Harry_Plotter.py ./path_to_10_FOLD_X-valid_results/ each\n"
    )
