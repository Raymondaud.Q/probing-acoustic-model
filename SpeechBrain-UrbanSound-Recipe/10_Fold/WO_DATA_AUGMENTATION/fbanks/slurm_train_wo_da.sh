#!/bin/bash

#SBATCH --job-name=US_10F
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpu
#SBATCH --gpus-per-node=4
#SBATCH --mem=8G
#SBATCH --time=5-00:00:00
#SBATCH --mail-type=ALL

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[2, 3, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[1] --test_fold_nums=[1] --output_folder=./results/urban_sound/fold_1 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 3, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[2] --test_fold_nums=[2] --output_folder=./results/urban_sound/fold_2 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[3] --test_fold_nums=[3] --output_folder=./results/urban_sound/fold_3 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[4] --test_fold_nums=[4] --output_folder=./results/urban_sound/fold_4 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 6, 7, 8, 9, 10] --valid_fold_nums=[5] --test_fold_nums=[5] --output_folder=./results/urban_sound/fold_5 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 5, 7, 8, 9, 10] --valid_fold_nums=[6] --test_fold_nums=[6] --output_folder=./results/urban_sound/fold_6 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 5, 6, 8, 9, 10] --valid_fold_nums=[7] --test_fold_nums=[7] --output_folder=./results/urban_sound/fold_7 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 9, 10] --valid_fold_nums=[8] --test_fold_nums=[8] --output_folder=./results/urban_sound/fold_8 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 8, 10] --valid_fold_nums=[9] --test_fold_nums=[9] --output_folder=./results/urban_sound/fold_9 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn_wo_augment.yaml --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 8, 9] --valid_fold_nums=[10] --test_fold_nums=[10] --output_folder=./results/urban_sound/fold_10 --data_parallel_backend

