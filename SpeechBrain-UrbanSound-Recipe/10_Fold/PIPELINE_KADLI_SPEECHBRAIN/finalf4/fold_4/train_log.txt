epoch: 1, lr: 4.19e-06 - train loss: 6.63, train acc: 4.56e-01 - valid loss: 5.09, valid acc: 6.36e-01, valid error: 3.64e-01
epoch: 2, lr: 8.39e-06 - train loss: 3.05, train acc: 8.00e-01 - valid loss: 5.24, valid acc: 6.06e-01, valid error: 3.94e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.89, train acc: 8.83e-01 - valid loss: 6.17, valid acc: 5.64e-01, valid error: 4.36e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.27, train acc: 9.24e-01 - valid loss: 4.33, valid acc: 7.05e-01, valid error: 2.95e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.07, train acc: 9.35e-01 - valid loss: 6.05, valid acc: 5.90e-01, valid error: 4.10e-01
epoch: 6, lr: 2.52e-05 - train loss: 9.43e-01, train acc: 9.42e-01 - valid loss: 4.85, valid acc: 6.77e-01, valid error: 3.23e-01
epoch: 7, lr: 2.94e-05 - train loss: 9.32e-01, train acc: 9.40e-01 - valid loss: 5.82, valid acc: 6.56e-01, valid error: 3.44e-01
epoch: 8, lr: 3.36e-05 - train loss: 8.88e-01, train acc: 9.42e-01 - valid loss: 5.55, valid acc: 6.52e-01, valid error: 3.48e-01
epoch: 9, lr: 3.78e-05 - train loss: 8.76e-01, train acc: 9.44e-01 - valid loss: 4.26, valid acc: 7.24e-01, valid error: 2.76e-01
epoch: 10, lr: 4.20e-05 - train loss: 8.07e-01, train acc: 9.47e-01 - valid loss: 5.88, valid acc: 6.30e-01, valid error: 3.70e-01
epoch: 11, lr: 4.62e-05 - train loss: 7.12e-01, train acc: 9.54e-01 - valid loss: 6.18, valid acc: 6.44e-01, valid error: 3.57e-01
epoch: 12, lr: 5.04e-05 - train loss: 6.88e-01, train acc: 9.55e-01 - valid loss: 5.97, valid acc: 6.75e-01, valid error: 3.25e-01
epoch: 13, lr: 5.46e-05 - train loss: 6.14e-01, train acc: 9.59e-01 - valid loss: 5.61, valid acc: 7.08e-01, valid error: 2.92e-01
epoch: 14, lr: 5.88e-05 - train loss: 6.15e-01, train acc: 9.60e-01 - valid loss: 5.10, valid acc: 7.12e-01, valid error: 2.88e-01
epoch: 15, lr: 6.30e-05 - train loss: 5.25e-01, train acc: 9.66e-01 - valid loss: 5.98, valid acc: 6.55e-01, valid error: 3.45e-01
epoch: 16, lr: 6.72e-05 - train loss: 4.88e-01, train acc: 9.67e-01 - valid loss: 5.66, valid acc: 6.94e-01, valid error: 3.06e-01
epoch: 17, lr: 7.14e-05 - train loss: 4.09e-01, train acc: 9.72e-01 - valid loss: 5.02, valid acc: 7.22e-01, valid error: 2.78e-01
epoch: 18, lr: 7.56e-05 - train loss: 4.35e-01, train acc: 9.73e-01 - valid loss: 5.93, valid acc: 6.71e-01, valid error: 3.29e-01
epoch: 19, lr: 7.98e-05 - train loss: 4.24e-01, train acc: 9.73e-01 - valid loss: 5.99, valid acc: 6.67e-01, valid error: 3.33e-01
epoch: 20, lr: 8.40e-05 - train loss: 3.81e-01, train acc: 9.76e-01 - valid loss: 5.47, valid acc: 6.93e-01, valid error: 3.07e-01
epoch: 21, lr: 8.82e-05 - train loss: 3.09e-01, train acc: 9.80e-01 - valid loss: 5.19, valid acc: 7.14e-01, valid error: 2.86e-01
epoch: 22, lr: 9.24e-05 - train loss: 3.46e-01, train acc: 9.76e-01 - valid loss: 5.41, valid acc: 6.96e-01, valid error: 3.04e-01
epoch: 23, lr: 9.66e-05 - train loss: 3.65e-01, train acc: 9.75e-01 - valid loss: 5.31, valid acc: 7.14e-01, valid error: 2.86e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.02e-01, train acc: 9.78e-01 - valid loss: 5.91, valid acc: 6.69e-01, valid error: 3.30e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.00e-01, train acc: 9.79e-01 - valid loss: 5.61, valid acc: 6.86e-01, valid error: 3.14e-01
Epoch loaded: 9, 
 Per Class Accuracy: 
0: 0.850
1: 0.800
2: 0.508
3: 0.910
4: 0.570
5: 0.789
6: 0.952
7: 0.430
8: 0.633
9: 0.640, 
 Confusion Matrix: 
[[ 85   5   0   5   3   0   1   0   1   0]
 [  2  80   1   1  10   0   0   1   0   5]
 [  5   2  30   6   0   0   0   0  14   2]
 [  1   3   0  91   0   0   0   2   0   3]
 [  0  14   0  11  57   0  14   2   2   0]
 [  5   0   1   0   0  30   0   0   2   0]
 [  4   0   0   0   0   0 158   4   0   0]
 [  0   1   0   3   2   0   0  46  52   3]
 [  8   0   0  20   2   0   1  11  76   2]
 [  5   4   3   8   0   0   0   9   7  64]]
 - test loss: 4.26, test acc: 7.24e-01, test error: 2.76e-01
