epoch: 1, lr: 4.19e-06 - train loss: 6.60, train acc: 4.61e-01 - valid loss: 5.84, valid acc: 5.15e-01, valid error: 4.85e-01
epoch: 2, lr: 8.39e-06 - train loss: 3.03, train acc: 8.04e-01 - valid loss: 5.79, valid acc: 5.60e-01, valid error: 4.40e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.80, train acc: 8.89e-01 - valid loss: 5.95, valid acc: 5.78e-01, valid error: 4.23e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.25, train acc: 9.28e-01 - valid loss: 5.64, valid acc: 6.06e-01, valid error: 3.95e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.21, train acc: 9.24e-01 - valid loss: 6.02, valid acc: 5.91e-01, valid error: 4.09e-01
epoch: 6, lr: 2.52e-05 - train loss: 9.54e-01, train acc: 9.42e-01 - valid loss: 7.25, valid acc: 5.79e-01, valid error: 4.22e-01
epoch: 7, lr: 2.94e-05 - train loss: 8.70e-01, train acc: 9.45e-01 - valid loss: 6.78, valid acc: 6.02e-01, valid error: 3.98e-01
epoch: 8, lr: 3.36e-05 - train loss: 9.03e-01, train acc: 9.40e-01 - valid loss: 6.27, valid acc: 5.89e-01, valid error: 4.11e-01
epoch: 9, lr: 3.78e-05 - train loss: 7.66e-01, train acc: 9.51e-01 - valid loss: 6.87, valid acc: 6.15e-01, valid error: 3.85e-01
epoch: 10, lr: 4.20e-05 - train loss: 7.27e-01, train acc: 9.53e-01 - valid loss: 7.13, valid acc: 5.62e-01, valid error: 4.38e-01
epoch: 11, lr: 4.62e-05 - train loss: 6.57e-01, train acc: 9.57e-01 - valid loss: 7.18, valid acc: 6.05e-01, valid error: 3.95e-01
epoch: 12, lr: 5.04e-05 - train loss: 6.38e-01, train acc: 9.58e-01 - valid loss: 7.40, valid acc: 5.87e-01, valid error: 4.13e-01
epoch: 13, lr: 5.46e-05 - train loss: 5.80e-01, train acc: 9.61e-01 - valid loss: 6.88, valid acc: 6.10e-01, valid error: 3.90e-01
epoch: 14, lr: 5.88e-05 - train loss: 5.18e-01, train acc: 9.66e-01 - valid loss: 6.74, valid acc: 6.45e-01, valid error: 3.56e-01
epoch: 15, lr: 6.30e-05 - train loss: 4.78e-01, train acc: 9.70e-01 - valid loss: 6.95, valid acc: 6.15e-01, valid error: 3.85e-01
epoch: 16, lr: 6.72e-05 - train loss: 5.15e-01, train acc: 9.68e-01 - valid loss: 7.55, valid acc: 5.81e-01, valid error: 4.19e-01
epoch: 17, lr: 7.14e-05 - train loss: 4.02e-01, train acc: 9.75e-01 - valid loss: 7.58, valid acc: 6.26e-01, valid error: 3.74e-01
epoch: 18, lr: 7.56e-05 - train loss: 4.11e-01, train acc: 9.73e-01 - valid loss: 7.31, valid acc: 6.27e-01, valid error: 3.73e-01
epoch: 19, lr: 7.98e-05 - train loss: 3.91e-01, train acc: 9.74e-01 - valid loss: 6.79, valid acc: 6.34e-01, valid error: 3.66e-01
epoch: 20, lr: 8.40e-05 - train loss: 3.89e-01, train acc: 9.75e-01 - valid loss: 7.49, valid acc: 6.24e-01, valid error: 3.76e-01
epoch: 21, lr: 8.82e-05 - train loss: 3.81e-01, train acc: 9.76e-01 - valid loss: 7.20, valid acc: 6.21e-01, valid error: 3.79e-01
epoch: 22, lr: 9.24e-05 - train loss: 3.43e-01, train acc: 9.77e-01 - valid loss: 6.74, valid acc: 6.24e-01, valid error: 3.76e-01
epoch: 23, lr: 9.66e-05 - train loss: 3.16e-01, train acc: 9.78e-01 - valid loss: 6.18, valid acc: 6.55e-01, valid error: 3.45e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.88e-01, train acc: 9.81e-01 - valid loss: 6.52, valid acc: 6.39e-01, valid error: 3.61e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.96e-01, train acc: 9.81e-01 - valid loss: 7.10, valid acc: 6.36e-01, valid error: 3.64e-01
Epoch loaded: 23, 
 Per Class Accuracy: 
0: 0.860
1: 0.880
2: 0.698
3: 0.300
4: 0.510
5: 1.000
6: 0.840
7: 0.467
8: 0.525
9: 0.720, 
 Confusion Matrix: 
[[ 86   7   0   2   3   0   2   0   0   0]
 [  0  88   0   2   5   0   3   1   0   1]
 [  0   0  30   0   3   0   0   0   0  10]
 [ 24   7   0  30  13   0   3  13   1   9]
 [  0  37   0   4  51   0   3   3   0   2]
 [  0   0   0   0   0  36   0   0   0   0]
 [  0   1   1   1  14   0 100   2   0   0]
 [  2   5   1   8   1   1   5  50  24  10]
 [  4   5   0  44   0   0   0   2  63   2]
 [  2   0   1   0   0   1   2  21   1  72]]
 - test loss: 6.18, test acc: 6.55e-01, test error: 3.45e-01
