epoch: 1, lr: 4.21e-06 - train loss: 6.87, train acc: 4.32e-01 - valid loss: 5.40, valid acc: 5.88e-01, valid error: 4.20e-01
epoch: 2, lr: 8.43e-06 - train loss: 3.57, train acc: 7.63e-01 - valid loss: 5.89, valid acc: 6.05e-01, valid error: 4.00e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.01, train acc: 8.77e-01 - valid loss: 5.20, valid acc: 6.47e-01, valid error: 3.56e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.42, train acc: 9.14e-01 - valid loss: 4.89, valid acc: 6.87e-01, valid error: 3.15e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.28, train acc: 9.21e-01 - valid loss: 5.65, valid acc: 6.66e-01, valid error: 3.40e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.06, train acc: 9.37e-01 - valid loss: 5.54, valid acc: 6.85e-01, valid error: 3.21e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.06, train acc: 9.35e-01 - valid loss: 5.61, valid acc: 6.80e-01, valid error: 3.25e-01
epoch: 8, lr: 3.37e-05 - train loss: 9.85e-01, train acc: 9.39e-01 - valid loss: 5.44, valid acc: 6.92e-01, valid error: 3.13e-01
epoch: 9, lr: 3.79e-05 - train loss: 8.55e-01, train acc: 9.44e-01 - valid loss: 6.54, valid acc: 6.32e-01, valid error: 3.75e-01
epoch: 10, lr: 4.21e-05 - train loss: 9.18e-01, train acc: 9.40e-01 - valid loss: 6.26, valid acc: 6.65e-01, valid error: 3.41e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.03e-01, train acc: 9.56e-01 - valid loss: 5.42, valid acc: 7.07e-01, valid error: 2.98e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.96e-01, train acc: 9.56e-01 - valid loss: 5.88, valid acc: 6.73e-01, valid error: 3.30e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.56e-01, train acc: 9.58e-01 - valid loss: 6.00, valid acc: 6.73e-01, valid error: 3.30e-01
epoch: 14, lr: 5.90e-05 - train loss: 6.22e-01, train acc: 9.58e-01 - valid loss: 7.06, valid acc: 6.61e-01, valid error: 3.42e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.87e-01, train acc: 9.60e-01 - valid loss: 6.43, valid acc: 6.68e-01, valid error: 3.38e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.75e-01, train acc: 9.61e-01 - valid loss: 6.27, valid acc: 6.53e-01, valid error: 3.51e-01
epoch: 17, lr: 7.17e-05 - train loss: 5.01e-01, train acc: 9.69e-01 - valid loss: 6.89, valid acc: 6.34e-01, valid error: 3.67e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.65e-01, train acc: 9.72e-01 - valid loss: 6.02, valid acc: 6.59e-01, valid error: 3.44e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.51e-01, train acc: 9.71e-01 - valid loss: 5.31, valid acc: 7.21e-01, valid error: 2.86e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.06e-01, train acc: 9.74e-01 - valid loss: 5.62, valid acc: 7.10e-01, valid error: 2.98e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.92e-01, train acc: 9.75e-01 - valid loss: 6.72, valid acc: 6.70e-01, valid error: 3.36e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.78e-01, train acc: 9.76e-01 - valid loss: 5.61, valid acc: 6.78e-01, valid error: 3.28e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.51e-01, train acc: 9.78e-01 - valid loss: 6.02, valid acc: 6.93e-01, valid error: 3.09e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.73e-01, train acc: 9.75e-01 - valid loss: 5.78, valid acc: 7.08e-01, valid error: 3.00e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.27e-01, train acc: 9.79e-01 - valid loss: 5.64, valid acc: 7.11e-01, valid error: 2.91e-01
Epoch loaded: 19, 
 Per Class Accuracy: 
0: 0.900
1: 0.930
2: 1.000
3: 0.640
4: 0.700
5: 0.953
6: 0.417
7: 0.530
8: 0.517
9: 0.943, 
 Confusion Matrix: 
[[90  4  2  1  0  2  0  0  1  0]
 [ 2 93  0  2  2  0  0  1  0  0]
 [ 0  0 36  0  0  0  0  0  0  0]
 [ 6 15  1 64  2  0  8  3  1  0]
 [ 1 15  2  2 70  4  0  4  2  0]
 [ 0  3  0  1  0 82  0  0  0  0]
 [ 0  9  1 22  0  3 40  0 21  0]
 [ 3  5 11 10  4  3  3 53  8  0]
 [ 0  2  2  1  0  0  5 48 62  0]
 [ 1  0  0  0  0  0  0  1  0 33]]
 - test loss: 5.31, test acc: 7.21e-01, test error: 2.86e-01
