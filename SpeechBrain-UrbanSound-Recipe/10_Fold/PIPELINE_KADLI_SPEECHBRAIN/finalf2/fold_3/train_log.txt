epoch: 1, lr: 4.19e-06 - train loss: 6.82, train acc: 4.48e-01 - valid loss: 6.01, valid acc: 4.81e-01, valid error: 5.19e-01
epoch: 2, lr: 8.39e-06 - train loss: 3.57, train acc: 7.65e-01 - valid loss: 5.50, valid acc: 5.75e-01, valid error: 4.25e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.02, train acc: 8.78e-01 - valid loss: 5.36, valid acc: 6.37e-01, valid error: 3.63e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.40, train acc: 9.15e-01 - valid loss: 5.40, valid acc: 6.58e-01, valid error: 3.43e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.23, train acc: 9.24e-01 - valid loss: 5.42, valid acc: 6.73e-01, valid error: 3.28e-01
epoch: 6, lr: 2.52e-05 - train loss: 1.14, train acc: 9.30e-01 - valid loss: 6.38, valid acc: 5.97e-01, valid error: 4.03e-01
epoch: 7, lr: 2.94e-05 - train loss: 1.01, train acc: 9.37e-01 - valid loss: 6.69, valid acc: 6.45e-01, valid error: 3.55e-01
epoch: 8, lr: 3.36e-05 - train loss: 1.06, train acc: 9.29e-01 - valid loss: 6.54, valid acc: 5.66e-01, valid error: 4.34e-01
epoch: 9, lr: 3.78e-05 - train loss: 8.47e-01, train acc: 9.44e-01 - valid loss: 6.81, valid acc: 5.83e-01, valid error: 4.17e-01
epoch: 10, lr: 4.20e-05 - train loss: 8.41e-01, train acc: 9.46e-01 - valid loss: 7.01, valid acc: 5.94e-01, valid error: 4.06e-01
epoch: 11, lr: 4.62e-05 - train loss: 7.43e-01, train acc: 9.51e-01 - valid loss: 6.53, valid acc: 6.23e-01, valid error: 3.77e-01
epoch: 12, lr: 5.04e-05 - train loss: 7.29e-01, train acc: 9.53e-01 - valid loss: 7.14, valid acc: 5.79e-01, valid error: 4.22e-01
epoch: 13, lr: 5.46e-05 - train loss: 6.24e-01, train acc: 9.60e-01 - valid loss: 6.34, valid acc: 6.48e-01, valid error: 3.51e-01
epoch: 14, lr: 5.88e-05 - train loss: 6.23e-01, train acc: 9.61e-01 - valid loss: 6.62, valid acc: 6.12e-01, valid error: 3.88e-01
epoch: 15, lr: 6.30e-05 - train loss: 5.75e-01, train acc: 9.61e-01 - valid loss: 8.60, valid acc: 5.93e-01, valid error: 4.08e-01
epoch: 16, lr: 6.72e-05 - train loss: 5.42e-01, train acc: 9.65e-01 - valid loss: 7.05, valid acc: 6.19e-01, valid error: 3.81e-01
epoch: 17, lr: 7.14e-05 - train loss: 5.75e-01, train acc: 9.61e-01 - valid loss: 7.88, valid acc: 6.00e-01, valid error: 4.00e-01
epoch: 18, lr: 7.56e-05 - train loss: 4.33e-01, train acc: 9.70e-01 - valid loss: 7.10, valid acc: 6.07e-01, valid error: 3.92e-01
epoch: 19, lr: 7.98e-05 - train loss: 4.31e-01, train acc: 9.73e-01 - valid loss: 6.28, valid acc: 6.44e-01, valid error: 3.56e-01
epoch: 20, lr: 8.40e-05 - train loss: 4.45e-01, train acc: 9.71e-01 - valid loss: 6.29, valid acc: 6.93e-01, valid error: 3.07e-01
epoch: 21, lr: 8.82e-05 - train loss: 4.02e-01, train acc: 9.73e-01 - valid loss: 6.55, valid acc: 6.52e-01, valid error: 3.48e-01
epoch: 22, lr: 9.24e-05 - train loss: 3.88e-01, train acc: 9.75e-01 - valid loss: 7.07, valid acc: 6.17e-01, valid error: 3.83e-01
epoch: 23, lr: 9.66e-05 - train loss: 3.46e-01, train acc: 9.77e-01 - valid loss: 6.17, valid acc: 6.43e-01, valid error: 3.57e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.47e-01, train acc: 9.78e-01 - valid loss: 7.26, valid acc: 6.46e-01, valid error: 3.55e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.15e-01, train acc: 9.82e-01 - valid loss: 8.15, valid acc: 5.62e-01, valid error: 4.38e-01
Epoch loaded: 20, 
 Per Class Accuracy: 
0: 0.890
1: 0.730
2: 0.628
3: 0.480
4: 0.750
5: 0.972
6: 0.815
7: 0.234
8: 0.900
9: 0.640, 
 Confusion Matrix: 
[[ 89   4   0   2   4   0   1   0   0   0]
 [  1  73   0   5  13   0   1   6   0   1]
 [  2   0  27   1   0   0   0   0   0  13]
 [ 23   0   0  48   8   0   1   7  10   3]
 [  0  12   1   6  75   0   2   1   0   3]
 [  1   0   0   0   0  35   0   0   0   0]
 [  0   0   1   3  14   0  97   2   2   0]
 [  3   8   4  11   0   2   1  25  49   4]
 [  0   0   2   4   4   0   0   0 108   2]
 [  3   0   6   3   0   1   1   9  13  64]]
 - test loss: 6.29, test acc: 6.93e-01, test error: 3.07e-01
