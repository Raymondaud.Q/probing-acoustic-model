epoch: 1, lr: 4.21e-06 - train loss: 6.49, train acc: 4.81e-01 - valid loss: 4.20, valid acc: 7.01e-01, valid error: 3.03e-01
epoch: 2, lr: 8.43e-06 - train loss: 2.95, train acc: 8.11e-01 - valid loss: 3.83, valid acc: 7.27e-01, valid error: 2.76e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.72, train acc: 8.89e-01 - valid loss: 4.59, valid acc: 7.06e-01, valid error: 2.97e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.22, train acc: 9.24e-01 - valid loss: 3.09, valid acc: 7.93e-01, valid error: 2.08e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.03, train acc: 9.36e-01 - valid loss: 3.17, valid acc: 8.02e-01, valid error: 1.99e-01
epoch: 6, lr: 2.53e-05 - train loss: 9.38e-01, train acc: 9.41e-01 - valid loss: 3.64, valid acc: 7.88e-01, valid error: 2.13e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.06e-01, train acc: 9.41e-01 - valid loss: 3.87, valid acc: 7.72e-01, valid error: 2.32e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.25e-01, train acc: 9.47e-01 - valid loss: 3.28, valid acc: 8.04e-01, valid error: 1.99e-01
epoch: 9, lr: 3.79e-05 - train loss: 8.35e-01, train acc: 9.47e-01 - valid loss: 4.10, valid acc: 7.72e-01, valid error: 2.29e-01
epoch: 10, lr: 4.21e-05 - train loss: 6.87e-01, train acc: 9.56e-01 - valid loss: 4.41, valid acc: 7.56e-01, valid error: 2.48e-01
epoch: 11, lr: 4.64e-05 - train loss: 6.79e-01, train acc: 9.56e-01 - valid loss: 3.83, valid acc: 7.72e-01, valid error: 2.30e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.84e-01, train acc: 9.57e-01 - valid loss: 3.71, valid acc: 7.76e-01, valid error: 2.25e-01
epoch: 13, lr: 5.48e-05 - train loss: 5.91e-01, train acc: 9.61e-01 - valid loss: 3.39, valid acc: 8.05e-01, valid error: 1.97e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.26e-01, train acc: 9.65e-01 - valid loss: 3.55, valid acc: 7.97e-01, valid error: 2.06e-01
epoch: 15, lr: 6.32e-05 - train loss: 4.91e-01, train acc: 9.65e-01 - valid loss: 3.45, valid acc: 8.03e-01, valid error: 1.99e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.48e-01, train acc: 9.70e-01 - valid loss: 3.53, valid acc: 8.04e-01, valid error: 1.99e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.72e-01, train acc: 9.69e-01 - valid loss: 3.85, valid acc: 8.09e-01, valid error: 1.94e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.03e-01, train acc: 9.73e-01 - valid loss: 3.71, valid acc: 8.00e-01, valid error: 2.02e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.89e-01, train acc: 9.76e-01 - valid loss: 4.09, valid acc: 7.98e-01, valid error: 2.05e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.77e-01, train acc: 9.75e-01 - valid loss: 4.00, valid acc: 7.94e-01, valid error: 2.08e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.56e-01, train acc: 9.76e-01 - valid loss: 3.80, valid acc: 7.91e-01, valid error: 2.11e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.17e-01, train acc: 9.80e-01 - valid loss: 4.51, valid acc: 7.67e-01, valid error: 2.37e-01
epoch: 23, lr: 9.69e-05 - train loss: 2.81e-01, train acc: 9.82e-01 - valid loss: 3.80, valid acc: 7.96e-01, valid error: 2.07e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.05e-01, train acc: 9.81e-01 - valid loss: 4.71, valid acc: 7.57e-01, valid error: 2.45e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.80e-01, train acc: 9.81e-01 - valid loss: 4.73, valid acc: 7.78e-01, valid error: 2.24e-01
Epoch loaded: 17, 
 Per Class Accuracy: 
0: 0.830
1: 0.760
2: 0.812
3: 0.640
4: 0.830
5: 0.968
6: 0.988
7: 0.820
8: 0.878
9: 0.700, 
 Confusion Matrix: 
[[83  1  3  4  3  1  0  2  1  2]
 [14 76  1  3  5  0  0  1  0  0]
 [ 3  0 26  1  0  0  0  0  0  2]
 [ 8  5  1 64 22  0  0  0  0  0]
 [ 1 13  0  2 83  0  1  0  0  0]
 [ 0  0  0  1  0 30  0  0  0  0]
 [ 0  1  0  0  0  0 81  0  0  0]
 [ 1  0  0 13  2  0  0 73  0  0]
 [ 0  0  1  0  0  0  0  0 72  9]
 [ 0  0  6  6  2  0  0  2 14 70]]
 - test loss: 3.85, test acc: 8.09e-01, test error: 1.94e-01
