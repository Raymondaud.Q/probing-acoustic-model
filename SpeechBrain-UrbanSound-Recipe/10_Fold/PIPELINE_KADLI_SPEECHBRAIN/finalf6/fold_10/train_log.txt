epoch: 1, lr: 4.21e-06 - train loss: 6.59, train acc: 4.60e-01 - valid loss: 4.16, valid acc: 7.32e-01, valid error: 2.70e-01
epoch: 2, lr: 8.43e-06 - train loss: 2.98, train acc: 8.07e-01 - valid loss: 4.35, valid acc: 7.36e-01, valid error: 2.66e-01
epoch: 3, lr: 1.26e-05 - train loss: 1.85, train acc: 8.82e-01 - valid loss: 3.81, valid acc: 7.19e-01, valid error: 2.83e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.32, train acc: 9.17e-01 - valid loss: 2.85, valid acc: 8.23e-01, valid error: 1.83e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.13, train acc: 9.30e-01 - valid loss: 3.04, valid acc: 7.84e-01, valid error: 2.16e-01
epoch: 6, lr: 2.53e-05 - train loss: 9.57e-01, train acc: 9.41e-01 - valid loss: 3.41, valid acc: 7.83e-01, valid error: 2.17e-01
epoch: 7, lr: 2.95e-05 - train loss: 9.13e-01, train acc: 9.42e-01 - valid loss: 4.14, valid acc: 7.73e-01, valid error: 2.28e-01
epoch: 8, lr: 3.37e-05 - train loss: 8.73e-01, train acc: 9.44e-01 - valid loss: 3.30, valid acc: 8.22e-01, valid error: 1.84e-01
epoch: 9, lr: 3.79e-05 - train loss: 8.10e-01, train acc: 9.47e-01 - valid loss: 3.29, valid acc: 8.02e-01, valid error: 2.04e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.97e-01, train acc: 9.49e-01 - valid loss: 3.51, valid acc: 8.00e-01, valid error: 2.07e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.34e-01, train acc: 9.53e-01 - valid loss: 3.82, valid acc: 7.84e-01, valid error: 2.16e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.15e-01, train acc: 9.62e-01 - valid loss: 3.32, valid acc: 8.21e-01, valid error: 1.85e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.64e-01, train acc: 9.57e-01 - valid loss: 3.49, valid acc: 8.14e-01, valid error: 1.92e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.55e-01, train acc: 9.64e-01 - valid loss: 3.49, valid acc: 8.12e-01, valid error: 1.88e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.21e-01, train acc: 9.66e-01 - valid loss: 3.67, valid acc: 8.00e-01, valid error: 2.00e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.07e-01, train acc: 9.65e-01 - valid loss: 3.49, valid acc: 8.11e-01, valid error: 1.95e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.36e-01, train acc: 9.70e-01 - valid loss: 3.41, valid acc: 8.30e-01, valid error: 1.76e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.40e-01, train acc: 9.72e-01 - valid loss: 3.06, valid acc: 8.26e-01, valid error: 1.73e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.22e-01, train acc: 9.72e-01 - valid loss: 2.96, valid acc: 8.29e-01, valid error: 1.70e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.53e-01, train acc: 9.78e-01 - valid loss: 3.32, valid acc: 8.35e-01, valid error: 1.64e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.52e-01, train acc: 9.77e-01 - valid loss: 3.13, valid acc: 8.32e-01, valid error: 1.73e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.78e-01, train acc: 9.76e-01 - valid loss: 4.16, valid acc: 7.65e-01, valid error: 2.37e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.21e-01, train acc: 9.78e-01 - valid loss: 3.88, valid acc: 8.05e-01, valid error: 1.95e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.92e-01, train acc: 9.80e-01 - valid loss: 3.60, valid acc: 8.24e-01, valid error: 1.76e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.08e-01, train acc: 9.80e-01 - valid loss: 3.59, valid acc: 8.10e-01, valid error: 1.90e-01
Epoch loaded: 20, 
 Per Class Accuracy: 
0: 0.900
1: 0.910
2: 0.740
3: 0.810
4: 1.000
5: 0.590
6: 0.860
7: 0.969
8: 0.800
9: 0.909, 
 Confusion Matrix: 
[[90  0  1  2  0  0  1  0  2  4]
 [ 0 91  2  4  0  1  1  0  1  0]
 [ 3  5 74  0  0  0 14  0  3  1]
 [ 1 11  0 81  0  3  0  0  4  0]
 [ 0  0  0  0 32  0  0  0  0  0]
 [20  4  0  5  0 49  0  1  1  3]
 [ 1  2  7  1  0  0 80  0  2  0]
 [ 0  0  0  0  2  0  1 93  0  0]
 [ 0  8  3  6  0  2  0  0 80  1]
 [ 1  2  0  0  0  0  0  0  0 30]]
 - test loss: 3.32, test acc: 8.35e-01, test error: 1.64e-01
