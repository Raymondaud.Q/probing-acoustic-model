epoch: 1, lr: 4.21e-06 - train loss: 7.34, train acc: 3.67e-01 - valid loss: 5.41, valid acc: 5.73e-01, valid error: 4.26e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.49, train acc: 6.84e-01 - valid loss: 5.65, valid acc: 6.15e-01, valid error: 3.86e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.76, train acc: 8.25e-01 - valid loss: 4.84, valid acc: 6.49e-01, valid error: 3.49e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.83, train acc: 8.93e-01 - valid loss: 4.00, valid acc: 7.21e-01, valid error: 2.78e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.49, train acc: 9.10e-01 - valid loss: 5.12, valid acc: 6.61e-01, valid error: 3.39e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.32, train acc: 9.21e-01 - valid loss: 4.54, valid acc: 7.07e-01, valid error: 2.93e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.26, train acc: 9.20e-01 - valid loss: 5.03, valid acc: 6.77e-01, valid error: 3.20e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.12, train acc: 9.30e-01 - valid loss: 4.50, valid acc: 7.04e-01, valid error: 2.94e-01
epoch: 9, lr: 3.79e-05 - train loss: 1.14, train acc: 9.27e-01 - valid loss: 5.04, valid acc: 6.92e-01, valid error: 3.05e-01
epoch: 10, lr: 4.21e-05 - train loss: 1.12, train acc: 9.29e-01 - valid loss: 6.43, valid acc: 6.83e-01, valid error: 3.19e-01
epoch: 11, lr: 4.64e-05 - train loss: 1.04, train acc: 9.30e-01 - valid loss: 3.95, valid acc: 7.51e-01, valid error: 2.50e-01
epoch: 12, lr: 5.06e-05 - train loss: 9.40e-01, train acc: 9.36e-01 - valid loss: 4.71, valid acc: 6.96e-01, valid error: 3.06e-01
epoch: 13, lr: 5.48e-05 - train loss: 7.64e-01, train acc: 9.48e-01 - valid loss: 4.08, valid acc: 7.31e-01, valid error: 2.68e-01
epoch: 14, lr: 5.90e-05 - train loss: 7.82e-01, train acc: 9.49e-01 - valid loss: 4.24, valid acc: 7.46e-01, valid error: 2.55e-01
epoch: 15, lr: 6.32e-05 - train loss: 6.81e-01, train acc: 9.56e-01 - valid loss: 4.20, valid acc: 7.48e-01, valid error: 2.55e-01
epoch: 16, lr: 6.74e-05 - train loss: 7.37e-01, train acc: 9.53e-01 - valid loss: 4.48, valid acc: 7.51e-01, valid error: 2.48e-01
epoch: 17, lr: 7.17e-05 - train loss: 6.31e-01, train acc: 9.60e-01 - valid loss: 4.44, valid acc: 7.50e-01, valid error: 2.50e-01
epoch: 18, lr: 7.59e-05 - train loss: 5.52e-01, train acc: 9.64e-01 - valid loss: 5.17, valid acc: 7.28e-01, valid error: 2.72e-01
epoch: 19, lr: 8.01e-05 - train loss: 5.58e-01, train acc: 9.64e-01 - valid loss: 4.10, valid acc: 7.79e-01, valid error: 2.21e-01
epoch: 20, lr: 8.43e-05 - train loss: 5.90e-01, train acc: 9.60e-01 - valid loss: 4.79, valid acc: 7.46e-01, valid error: 2.55e-01
epoch: 21, lr: 8.85e-05 - train loss: 4.89e-01, train acc: 9.67e-01 - valid loss: 4.64, valid acc: 7.49e-01, valid error: 2.54e-01
epoch: 22, lr: 9.27e-05 - train loss: 4.41e-01, train acc: 9.70e-01 - valid loss: 4.03, valid acc: 7.82e-01, valid error: 2.19e-01
epoch: 23, lr: 9.69e-05 - train loss: 4.10e-01, train acc: 9.73e-01 - valid loss: 4.10, valid acc: 7.66e-01, valid error: 2.34e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.86e-01, train acc: 9.74e-01 - valid loss: 4.63, valid acc: 7.45e-01, valid error: 2.55e-01
epoch: 25, lr: 1.05e-04 - train loss: 4.33e-01, train acc: 9.72e-01 - valid loss: 4.23, valid acc: 7.61e-01, valid error: 2.39e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.780
1: 0.780
2: 0.938
3: 0.620
4: 0.860
5: 0.968
6: 0.976
7: 0.663
8: 0.902
9: 0.600, 
 Confusion Matrix: 
[[78  4  1  5  0  2  1  6  0  3]
 [12 78  0  2  3  0  1  1  1  2]
 [ 1  0 30  0  1  0  0  0  0  0]
 [ 1  3  0 62 14  0  4  2  4 10]
 [ 0  9  0  0 86  0  3  1  0  1]
 [ 1  0  0  0  0 30  0  0  0  0]
 [ 1  0  0  0  0  0 80  0  1  0]
 [ 0  0  0 16  4  0  4 59  5  1]
 [ 0  0  0  4  0  0  0  1 74  3]
 [ 2  0  0 10  1  0  5  9 13 60]]
 - test loss: 4.03, test acc: 7.82e-01, test error: 2.19e-01
