epoch: 1, lr: 4.19e-06 - train loss: 7.27, train acc: 3.86e-01 - valid loss: 6.49, valid acc: 4.45e-01, valid error: 5.56e-01
epoch: 2, lr: 8.39e-06 - train loss: 4.38, train acc: 6.94e-01 - valid loss: 6.34, valid acc: 5.15e-01, valid error: 4.85e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.65, train acc: 8.31e-01 - valid loss: 6.23, valid acc: 5.53e-01, valid error: 4.48e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.85, train acc: 8.88e-01 - valid loss: 7.03, valid acc: 4.76e-01, valid error: 5.24e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.51, train acc: 9.09e-01 - valid loss: 6.37, valid acc: 5.51e-01, valid error: 4.50e-01
epoch: 6, lr: 2.52e-05 - train loss: 1.46, train acc: 9.11e-01 - valid loss: 6.73, valid acc: 5.44e-01, valid error: 4.56e-01
epoch: 7, lr: 2.94e-05 - train loss: 1.28, train acc: 9.18e-01 - valid loss: 7.05, valid acc: 5.63e-01, valid error: 4.37e-01
epoch: 8, lr: 3.36e-05 - train loss: 1.20, train acc: 9.22e-01 - valid loss: 7.51, valid acc: 5.08e-01, valid error: 4.92e-01
epoch: 9, lr: 3.78e-05 - train loss: 1.12, train acc: 9.28e-01 - valid loss: 6.94, valid acc: 5.28e-01, valid error: 4.72e-01
epoch: 10, lr: 4.20e-05 - train loss: 1.01, train acc: 9.36e-01 - valid loss: 8.46, valid acc: 5.39e-01, valid error: 4.62e-01
epoch: 11, lr: 4.62e-05 - train loss: 9.88e-01, train acc: 9.34e-01 - valid loss: 8.52, valid acc: 5.29e-01, valid error: 4.71e-01
epoch: 12, lr: 5.04e-05 - train loss: 8.57e-01, train acc: 9.47e-01 - valid loss: 7.29, valid acc: 5.59e-01, valid error: 4.41e-01
epoch: 13, lr: 5.46e-05 - train loss: 8.04e-01, train acc: 9.48e-01 - valid loss: 9.50, valid acc: 5.25e-01, valid error: 4.76e-01
epoch: 14, lr: 5.88e-05 - train loss: 7.27e-01, train acc: 9.52e-01 - valid loss: 8.41, valid acc: 5.05e-01, valid error: 4.95e-01
epoch: 15, lr: 6.30e-05 - train loss: 7.20e-01, train acc: 9.54e-01 - valid loss: 8.42, valid acc: 5.43e-01, valid error: 4.57e-01
epoch: 16, lr: 6.72e-05 - train loss: 6.65e-01, train acc: 9.56e-01 - valid loss: 8.70, valid acc: 5.07e-01, valid error: 4.93e-01
epoch: 17, lr: 7.14e-05 - train loss: 6.23e-01, train acc: 9.61e-01 - valid loss: 9.67, valid acc: 5.34e-01, valid error: 4.66e-01
epoch: 18, lr: 7.56e-05 - train loss: 5.59e-01, train acc: 9.63e-01 - valid loss: 9.64, valid acc: 5.52e-01, valid error: 4.49e-01
epoch: 19, lr: 7.98e-05 - train loss: 5.37e-01, train acc: 9.65e-01 - valid loss: 8.12, valid acc: 5.40e-01, valid error: 4.61e-01
epoch: 20, lr: 8.40e-05 - train loss: 4.95e-01, train acc: 9.67e-01 - valid loss: 8.48, valid acc: 5.36e-01, valid error: 4.64e-01
epoch: 21, lr: 8.82e-05 - train loss: 4.24e-01, train acc: 9.72e-01 - valid loss: 9.60, valid acc: 5.13e-01, valid error: 4.88e-01
epoch: 22, lr: 9.24e-05 - train loss: 4.67e-01, train acc: 9.70e-01 - valid loss: 8.47, valid acc: 5.76e-01, valid error: 4.25e-01
epoch: 23, lr: 9.66e-05 - train loss: 4.25e-01, train acc: 9.74e-01 - valid loss: 8.83, valid acc: 5.36e-01, valid error: 4.64e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.93e-01, train acc: 9.75e-01 - valid loss: 8.48, valid acc: 5.32e-01, valid error: 4.68e-01
epoch: 25, lr: 1.05e-04 - train loss: 4.05e-01, train acc: 9.74e-01 - valid loss: 9.50, valid acc: 5.50e-01, valid error: 4.50e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.900
1: 0.620
2: 0.465
3: 0.560
4: 0.790
5: 1.000
6: 0.706
7: 0.084
8: 0.367
9: 0.520, 
 Confusion Matrix: 
[[90  1  0  2  2  0  2  0  3  0]
 [ 3 62  0  6 20  0  2  1  2  4]
 [ 3  0 20  2  1  1  2  0  0 14]
 [10  0  0 56  3  0  0 15  4 12]
 [ 0  6  1  8 79  0  0  2  0  4]
 [ 0  0  0  0  0 36  0  0  0  0]
 [ 2  4  1 17  9  0 84  1  1  0]
 [ 2  2  1 40  2  1  0  9 47  3]
 [ 0  0  0 62  1  0  1  1 44 11]
 [ 2  0  2 19  5  0  0 10 10 52]]
 - test loss: 8.47, test acc: 5.76e-01, test error: 4.25e-01
