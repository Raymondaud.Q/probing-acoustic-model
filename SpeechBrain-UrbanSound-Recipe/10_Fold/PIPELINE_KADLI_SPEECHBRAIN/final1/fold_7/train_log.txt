epoch: 1, lr: 4.21e-06 - train loss: 7.15, train acc: 3.92e-01 - valid loss: 6.29, valid acc: 4.98e-01, valid error: 4.96e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.33, train acc: 7.00e-01 - valid loss: 5.66, valid acc: 5.61e-01, valid error: 4.43e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.67, train acc: 8.28e-01 - valid loss: 5.84, valid acc: 5.59e-01, valid error: 4.44e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.81, train acc: 8.89e-01 - valid loss: 5.36, valid acc: 6.26e-01, valid error: 3.85e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.54, train acc: 9.07e-01 - valid loss: 5.84, valid acc: 6.14e-01, valid error: 3.93e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.39, train acc: 9.11e-01 - valid loss: 6.08, valid acc: 6.17e-01, valid error: 3.95e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.19, train acc: 9.26e-01 - valid loss: 6.26, valid acc: 5.98e-01, valid error: 4.09e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.17, train acc: 9.23e-01 - valid loss: 6.16, valid acc: 6.21e-01, valid error: 3.85e-01
epoch: 9, lr: 3.79e-05 - train loss: 1.05, train acc: 9.34e-01 - valid loss: 5.60, valid acc: 6.39e-01, valid error: 3.68e-01
epoch: 10, lr: 4.21e-05 - train loss: 9.80e-01, train acc: 9.35e-01 - valid loss: 6.89, valid acc: 5.84e-01, valid error: 4.24e-01
epoch: 11, lr: 4.64e-05 - train loss: 1.00, train acc: 9.34e-01 - valid loss: 5.64, valid acc: 6.63e-01, valid error: 3.42e-01
epoch: 12, lr: 5.06e-05 - train loss: 8.83e-01, train acc: 9.42e-01 - valid loss: 6.74, valid acc: 6.00e-01, valid error: 4.07e-01
epoch: 13, lr: 5.48e-05 - train loss: 8.57e-01, train acc: 9.45e-01 - valid loss: 6.65, valid acc: 5.97e-01, valid error: 4.11e-01
epoch: 14, lr: 5.90e-05 - train loss: 7.29e-01, train acc: 9.47e-01 - valid loss: 6.04, valid acc: 6.51e-01, valid error: 3.54e-01
epoch: 15, lr: 6.32e-05 - train loss: 6.89e-01, train acc: 9.53e-01 - valid loss: 5.81, valid acc: 6.35e-01, valid error: 3.76e-01
epoch: 16, lr: 6.74e-05 - train loss: 6.49e-01, train acc: 9.56e-01 - valid loss: 5.94, valid acc: 6.45e-01, valid error: 3.60e-01
epoch: 17, lr: 7.17e-05 - train loss: 6.02e-01, train acc: 9.62e-01 - valid loss: 6.13, valid acc: 6.55e-01, valid error: 3.56e-01
epoch: 18, lr: 7.59e-05 - train loss: 5.31e-01, train acc: 9.63e-01 - valid loss: 6.77, valid acc: 6.10e-01, valid error: 4.02e-01
epoch: 19, lr: 8.01e-05 - train loss: 5.16e-01, train acc: 9.67e-01 - valid loss: 6.17, valid acc: 6.11e-01, valid error: 3.90e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.98e-01, train acc: 9.67e-01 - valid loss: 6.06, valid acc: 6.63e-01, valid error: 3.42e-01
epoch: 21, lr: 8.85e-05 - train loss: 4.82e-01, train acc: 9.69e-01 - valid loss: 6.05, valid acc: 6.55e-01, valid error: 3.56e-01
epoch: 22, lr: 9.27e-05 - train loss: 4.57e-01, train acc: 9.68e-01 - valid loss: 6.20, valid acc: 6.72e-01, valid error: 3.33e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.96e-01, train acc: 9.74e-01 - valid loss: 6.04, valid acc: 6.37e-01, valid error: 3.64e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.23e-01, train acc: 9.78e-01 - valid loss: 7.07, valid acc: 6.38e-01, valid error: 3.74e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.68e-01, train acc: 9.76e-01 - valid loss: 7.50, valid acc: 6.21e-01, valid error: 3.85e-01
Epoch loaded: 22, 
 Per Class Accuracy: 
0: 0.860
1: 0.710
2: 0.857
3: 0.920
4: 0.902
5: 0.462
6: 0.545
7: 0.197
8: 0.540
9: 0.800, 
 Confusion Matrix: 
[[86  2  1  2  0  1  1  1  2  4]
 [ 0 71  0 15  0  0  1  4  2  7]
 [ 0  0 24  0  1  1  0  1  0  1]
 [ 1  0  0 92  0  1  0  2  4  0]
 [ 1  0  0  0 46  0  0  4  0  0]
 [ 3  2  3 42  0 49  0  6  1  0]
 [ 8  9  0  6  0  1 42  1  1  9]
 [ 1  0  0 39  0  2  0 15 19  0]
 [ 1  1  0 30  0  0  2  9 54  3]
 [ 0 14  0  3  0  0  0  0  3 80]]
 - test loss: 6.20, test acc: 6.72e-01, test error: 3.33e-01
