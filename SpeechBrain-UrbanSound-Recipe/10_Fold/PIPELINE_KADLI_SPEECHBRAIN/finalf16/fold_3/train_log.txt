epoch: 1, lr: 4.19e-06 - train loss: 6.74, train acc: 4.53e-01 - valid loss: 5.79, valid acc: 5.19e-01, valid error: 4.81e-01
epoch: 2, lr: 8.39e-06 - train loss: 3.47, train acc: 7.72e-01 - valid loss: 5.84, valid acc: 5.50e-01, valid error: 4.51e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.20, train acc: 8.60e-01 - valid loss: 6.17, valid acc: 5.67e-01, valid error: 4.34e-01
epoch: 4, lr: 1.68e-05 - train loss: 1.75, train acc: 8.86e-01 - valid loss: 6.14, valid acc: 5.40e-01, valid error: 4.61e-01
epoch: 5, lr: 2.10e-05 - train loss: 1.41, train acc: 9.12e-01 - valid loss: 6.00, valid acc: 5.74e-01, valid error: 4.26e-01
epoch: 6, lr: 2.52e-05 - train loss: 1.23, train acc: 9.24e-01 - valid loss: 6.38, valid acc: 5.89e-01, valid error: 4.11e-01
epoch: 7, lr: 2.94e-05 - train loss: 1.10, train acc: 9.31e-01 - valid loss: 6.94, valid acc: 5.86e-01, valid error: 4.14e-01
epoch: 8, lr: 3.36e-05 - train loss: 1.05, train acc: 9.33e-01 - valid loss: 7.80, valid acc: 5.51e-01, valid error: 4.49e-01
epoch: 9, lr: 3.78e-05 - train loss: 9.31e-01, train acc: 9.39e-01 - valid loss: 6.27, valid acc: 5.80e-01, valid error: 4.21e-01
epoch: 10, lr: 4.20e-05 - train loss: 8.85e-01, train acc: 9.43e-01 - valid loss: 6.70, valid acc: 5.86e-01, valid error: 4.14e-01
epoch: 11, lr: 4.62e-05 - train loss: 7.55e-01, train acc: 9.53e-01 - valid loss: 6.08, valid acc: 6.16e-01, valid error: 3.84e-01
epoch: 12, lr: 5.04e-05 - train loss: 7.60e-01, train acc: 9.50e-01 - valid loss: 8.50, valid acc: 5.29e-01, valid error: 4.71e-01
epoch: 13, lr: 5.46e-05 - train loss: 6.57e-01, train acc: 9.58e-01 - valid loss: 8.14, valid acc: 5.54e-01, valid error: 4.46e-01
epoch: 14, lr: 5.88e-05 - train loss: 6.35e-01, train acc: 9.57e-01 - valid loss: 6.91, valid acc: 5.76e-01, valid error: 4.24e-01
epoch: 15, lr: 6.30e-05 - train loss: 6.38e-01, train acc: 9.57e-01 - valid loss: 7.68, valid acc: 5.56e-01, valid error: 4.44e-01
epoch: 16, lr: 6.72e-05 - train loss: 5.55e-01, train acc: 9.64e-01 - valid loss: 7.40, valid acc: 5.81e-01, valid error: 4.19e-01
epoch: 17, lr: 7.14e-05 - train loss: 5.49e-01, train acc: 9.65e-01 - valid loss: 6.94, valid acc: 5.82e-01, valid error: 4.18e-01
epoch: 18, lr: 7.56e-05 - train loss: 4.93e-01, train acc: 9.68e-01 - valid loss: 8.11, valid acc: 5.73e-01, valid error: 4.27e-01
epoch: 19, lr: 7.98e-05 - train loss: 4.46e-01, train acc: 9.73e-01 - valid loss: 8.28, valid acc: 5.71e-01, valid error: 4.29e-01
epoch: 20, lr: 8.40e-05 - train loss: 4.38e-01, train acc: 9.71e-01 - valid loss: 7.34, valid acc: 5.86e-01, valid error: 4.14e-01
epoch: 21, lr: 8.82e-05 - train loss: 4.97e-01, train acc: 9.68e-01 - valid loss: 7.22, valid acc: 6.00e-01, valid error: 4.00e-01
epoch: 22, lr: 9.24e-05 - train loss: 4.29e-01, train acc: 9.70e-01 - valid loss: 7.81, valid acc: 5.77e-01, valid error: 4.23e-01
epoch: 23, lr: 9.66e-05 - train loss: 3.92e-01, train acc: 9.73e-01 - valid loss: 7.86, valid acc: 5.95e-01, valid error: 4.05e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.77e-01, train acc: 9.76e-01 - valid loss: 8.70, valid acc: 5.67e-01, valid error: 4.34e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.84e-01, train acc: 9.74e-01 - valid loss: 8.26, valid acc: 5.67e-01, valid error: 4.34e-01
Epoch loaded: 11, 
 Per Class Accuracy: 
0: 0.810
1: 0.720
2: 0.558
3: 0.080
4: 0.630
5: 1.000
6: 0.782
7: 0.598
8: 0.583
9: 0.590, 
 Confusion Matrix: 
[[81  9  1  3  4  0  1  0  0  1]
 [ 6 72  0  2  8  0  2  8  0  2]
 [ 0  0 24  4  0  0  6  0  0  9]
 [11 10  0  8 20  0  8 32  7  4]
 [ 3 20  3  3 63  0  1  4  1  2]
 [ 0  0  0  0  0 36  0  0  0  0]
 [ 2  1  1  3 15  0 93  3  1  0]
 [ 2  1  0  8  1  1  0 64 29  1]
 [ 0  0  0 30  2  0  0 15 70  3]
 [ 9  0  4 12  1  0  2  6  7 59]]
 - test loss: 6.08, test acc: 6.16e-01, test error: 3.84e-01
