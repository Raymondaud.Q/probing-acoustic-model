epoch: 1, lr: 4.21e-06 - train loss: 6.63, train acc: 4.59e-01 - valid loss: 5.46, valid acc: 6.08e-01, valid error: 3.91e-01
epoch: 2, lr: 8.43e-06 - train loss: 3.39, train acc: 7.75e-01 - valid loss: 5.55, valid acc: 6.24e-01, valid error: 3.76e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.08, train acc: 8.72e-01 - valid loss: 5.58, valid acc: 6.38e-01, valid error: 3.61e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.56, train acc: 9.09e-01 - valid loss: 5.90, valid acc: 6.53e-01, valid error: 3.47e-01
epoch: 5, lr: 2.11e-05 - train loss: 1.24, train acc: 9.25e-01 - valid loss: 6.20, valid acc: 6.30e-01, valid error: 3.69e-01
epoch: 6, lr: 2.53e-05 - train loss: 1.11, train acc: 9.32e-01 - valid loss: 6.27, valid acc: 6.74e-01, valid error: 3.25e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.07, train acc: 9.32e-01 - valid loss: 5.78, valid acc: 6.16e-01, valid error: 3.84e-01
epoch: 8, lr: 3.37e-05 - train loss: 9.96e-01, train acc: 9.39e-01 - valid loss: 5.93, valid acc: 6.57e-01, valid error: 3.42e-01
epoch: 9, lr: 3.79e-05 - train loss: 9.58e-01, train acc: 9.37e-01 - valid loss: 8.06, valid acc: 6.29e-01, valid error: 3.70e-01
epoch: 10, lr: 4.21e-05 - train loss: 7.76e-01, train acc: 9.50e-01 - valid loss: 6.02, valid acc: 6.63e-01, valid error: 3.38e-01
epoch: 11, lr: 4.64e-05 - train loss: 7.43e-01, train acc: 9.51e-01 - valid loss: 7.03, valid acc: 6.28e-01, valid error: 3.72e-01
epoch: 12, lr: 5.06e-05 - train loss: 6.88e-01, train acc: 9.56e-01 - valid loss: 6.46, valid acc: 6.39e-01, valid error: 3.60e-01
epoch: 13, lr: 5.48e-05 - train loss: 6.73e-01, train acc: 9.57e-01 - valid loss: 7.14, valid acc: 6.35e-01, valid error: 3.65e-01
epoch: 14, lr: 5.90e-05 - train loss: 5.64e-01, train acc: 9.61e-01 - valid loss: 6.71, valid acc: 6.59e-01, valid error: 3.41e-01
epoch: 15, lr: 6.32e-05 - train loss: 5.89e-01, train acc: 9.63e-01 - valid loss: 6.12, valid acc: 6.85e-01, valid error: 3.14e-01
epoch: 16, lr: 6.74e-05 - train loss: 5.47e-01, train acc: 9.66e-01 - valid loss: 5.82, valid acc: 6.57e-01, valid error: 3.43e-01
epoch: 17, lr: 7.17e-05 - train loss: 4.81e-01, train acc: 9.68e-01 - valid loss: 6.38, valid acc: 6.66e-01, valid error: 3.33e-01
epoch: 18, lr: 7.59e-05 - train loss: 4.62e-01, train acc: 9.69e-01 - valid loss: 5.82, valid acc: 6.69e-01, valid error: 3.30e-01
epoch: 19, lr: 8.01e-05 - train loss: 4.46e-01, train acc: 9.71e-01 - valid loss: 6.05, valid acc: 6.80e-01, valid error: 3.20e-01
epoch: 20, lr: 8.43e-05 - train loss: 4.24e-01, train acc: 9.72e-01 - valid loss: 6.48, valid acc: 6.53e-01, valid error: 3.47e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.63e-01, train acc: 9.77e-01 - valid loss: 6.50, valid acc: 6.72e-01, valid error: 3.28e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.78e-01, train acc: 9.76e-01 - valid loss: 6.28, valid acc: 6.80e-01, valid error: 3.20e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.09e-01, train acc: 9.80e-01 - valid loss: 6.56, valid acc: 6.71e-01, valid error: 3.29e-01
epoch: 24, lr: 1.01e-04 - train loss: 3.72e-01, train acc: 9.75e-01 - valid loss: 5.96, valid acc: 6.80e-01, valid error: 3.19e-01
epoch: 25, lr: 1.05e-04 - train loss: 3.27e-01, train acc: 9.80e-01 - valid loss: 6.64, valid acc: 6.79e-01, valid error: 3.21e-01
Epoch loaded: 15, 
 Per Class Accuracy: 
0: 0.900
1: 0.830
2: 0.714
3: 0.350
4: 0.740
5: 0.971
6: 0.681
7: 0.850
8: 0.283
9: 0.820, 
 Confusion Matrix: 
[[90  2  6  0  1  1  0  0  0  0]
 [ 1 83  0  4  9  0  1  2  0  0]
 [ 5  0 30  0  4  0  2  0  0  1]
 [ 1  0  0 35  3  0  0 55  6  0]
 [ 1 13  4  1 74  0  3  1  1  2]
 [ 1  0  0  0  0 34  0  0  0  0]
 [ 4  8  1  0  1  0 62 14  0  1]
 [ 0  3  1  2  8  0  0 85  1  0]
 [ 0  0  0  1  3  1  4 70 34  7]
 [ 1  1  1  0  0  0  0  0 15 82]]
 - test loss: 6.12, test acc: 6.85e-01, test error: 3.14e-01
