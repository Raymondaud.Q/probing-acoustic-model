epoch: 1, lr: 4.21e-06 - train loss: 7.69, train acc: 3.26e-01 - valid loss: 5.65, valid acc: 5.93e-01, valid error: 4.06e-01
epoch: 2, lr: 8.43e-06 - train loss: 5.93, train acc: 5.48e-01 - valid loss: 4.51, valid acc: 6.73e-01, valid error: 3.28e-01
epoch: 3, lr: 1.26e-05 - train loss: 5.20, train acc: 6.17e-01 - valid loss: 4.14, valid acc: 7.03e-01, valid error: 2.94e-01
epoch: 4, lr: 1.69e-05 - train loss: 4.90, train acc: 6.40e-01 - valid loss: 4.24, valid acc: 6.89e-01, valid error: 3.06e-01
epoch: 5, lr: 2.11e-05 - train loss: 4.70, train acc: 6.56e-01 - valid loss: 3.79, valid acc: 7.28e-01, valid error: 2.73e-01
epoch: 6, lr: 2.53e-05 - train loss: 4.79, train acc: 6.50e-01 - valid loss: 3.89, valid acc: 7.01e-01, valid error: 2.95e-01
epoch: 7, lr: 2.95e-05 - train loss: 4.69, train acc: 6.50e-01 - valid loss: 3.77, valid acc: 7.28e-01, valid error: 2.71e-01
epoch: 8, lr: 3.37e-05 - train loss: 4.63, train acc: 6.58e-01 - valid loss: 3.24, valid acc: 7.73e-01, valid error: 2.23e-01
epoch: 9, lr: 3.79e-05 - train loss: 4.52, train acc: 6.62e-01 - valid loss: 3.48, valid acc: 7.49e-01, valid error: 2.49e-01
epoch: 10, lr: 4.21e-05 - train loss: 4.40, train acc: 6.74e-01 - valid loss: 3.79, valid acc: 7.16e-01, valid error: 2.82e-01
epoch: 11, lr: 4.64e-05 - train loss: 4.23, train acc: 6.88e-01 - valid loss: 3.84, valid acc: 7.24e-01, valid error: 2.75e-01
epoch: 12, lr: 5.06e-05 - train loss: 4.19, train acc: 6.89e-01 - valid loss: 3.18, valid acc: 7.70e-01, valid error: 2.27e-01
epoch: 13, lr: 5.48e-05 - train loss: 3.99, train acc: 7.08e-01 - valid loss: 3.07, valid acc: 7.73e-01, valid error: 2.23e-01
epoch: 14, lr: 5.90e-05 - train loss: 3.84, train acc: 7.17e-01 - valid loss: 2.67, valid acc: 8.14e-01, valid error: 1.84e-01
epoch: 15, lr: 6.32e-05 - train loss: 3.69, train acc: 7.27e-01 - valid loss: 3.13, valid acc: 7.63e-01, valid error: 2.33e-01
epoch: 16, lr: 6.74e-05 - train loss: 3.59, train acc: 7.37e-01 - valid loss: 2.36, valid acc: 8.34e-01, valid error: 1.64e-01
epoch: 17, lr: 7.17e-05 - train loss: 3.38, train acc: 7.56e-01 - valid loss: 2.53, valid acc: 8.11e-01, valid error: 1.88e-01
epoch: 18, lr: 7.59e-05 - train loss: 3.22, train acc: 7.64e-01 - valid loss: 2.27, valid acc: 8.33e-01, valid error: 1.67e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.10, train acc: 7.75e-01 - valid loss: 1.99, valid acc: 8.56e-01, valid error: 1.42e-01
epoch: 20, lr: 8.43e-05 - train loss: 2.88, train acc: 7.89e-01 - valid loss: 2.35, valid acc: 8.32e-01, valid error: 1.65e-01
epoch: 21, lr: 8.85e-05 - train loss: 2.72, train acc: 8.02e-01 - valid loss: 2.00, valid acc: 8.64e-01, valid error: 1.34e-01
epoch: 22, lr: 9.27e-05 - train loss: 2.58, train acc: 8.10e-01 - valid loss: 1.84, valid acc: 8.71e-01, valid error: 1.27e-01
epoch: 23, lr: 9.69e-05 - train loss: 2.47, train acc: 8.20e-01 - valid loss: 2.00, valid acc: 8.45e-01, valid error: 1.52e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.32, train acc: 8.33e-01 - valid loss: 1.40, valid acc: 8.99e-01, valid error: 1.00e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.07, train acc: 8.49e-01 - valid loss: 1.16, valid acc: 9.10e-01, valid error: 8.70e-02
Epoch loaded: 25, 
 Per Class Accuracy: 
0: 0.900
1: 0.950
2: 0.781
3: 0.940
4: 0.930
5: 0.968
6: 0.890
7: 1.000
8: 0.915
9: 0.810, 
 Confusion Matrix: 
[[90  6  1  1  1  0  0  0  0  1]
 [ 0 95  0  3  2  0  0  0  0  0]
 [ 0  0 25  0  0  4  0  1  0  2]
 [ 0  3  0 94  2  0  0  0  0  1]
 [ 0  6  0  0 93  0  1  0  0  0]
 [ 0  0  0  0  0 30  0  0  1  0]
 [ 1  3  0  0  1  1 73  1  1  1]
 [ 0  0  0  0  0  0  0 89  0  0]
 [ 0  0  0  1  1  0  0  4 75  1]
 [ 3  1  0  2  1  1  0  2  9 81]]
 - test loss: 1.16, test acc: 9.10e-01, test error: 8.70e-02
