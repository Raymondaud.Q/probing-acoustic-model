epoch: 1, lr: 4.19e-06 - train loss: 7.81, train acc: 3.05e-01 - valid loss: 7.18, valid acc: 4.03e-01, valid error: 5.97e-01
epoch: 2, lr: 8.39e-06 - train loss: 6.05, train acc: 5.32e-01 - valid loss: 7.11, valid acc: 4.34e-01, valid error: 5.66e-01
epoch: 3, lr: 1.26e-05 - train loss: 5.22, train acc: 6.13e-01 - valid loss: 7.06, valid acc: 4.58e-01, valid error: 5.42e-01
epoch: 4, lr: 1.68e-05 - train loss: 4.76, train acc: 6.51e-01 - valid loss: 6.97, valid acc: 4.84e-01, valid error: 5.16e-01
epoch: 5, lr: 2.10e-05 - train loss: 4.37, train acc: 6.82e-01 - valid loss: 7.43, valid acc: 4.22e-01, valid error: 5.78e-01
epoch: 6, lr: 2.52e-05 - train loss: 4.09, train acc: 6.98e-01 - valid loss: 7.38, valid acc: 4.43e-01, valid error: 5.57e-01
epoch: 7, lr: 2.94e-05 - train loss: 4.00, train acc: 7.05e-01 - valid loss: 7.79, valid acc: 4.40e-01, valid error: 5.60e-01
epoch: 8, lr: 3.36e-05 - train loss: 3.72, train acc: 7.26e-01 - valid loss: 7.61, valid acc: 4.58e-01, valid error: 5.42e-01
epoch: 9, lr: 3.78e-05 - train loss: 3.54, train acc: 7.43e-01 - valid loss: 7.81, valid acc: 4.81e-01, valid error: 5.19e-01
epoch: 10, lr: 4.20e-05 - train loss: 3.40, train acc: 7.52e-01 - valid loss: 7.67, valid acc: 4.75e-01, valid error: 5.25e-01
epoch: 11, lr: 4.62e-05 - train loss: 3.36, train acc: 7.55e-01 - valid loss: 9.21, valid acc: 4.10e-01, valid error: 5.90e-01
epoch: 12, lr: 5.04e-05 - train loss: 3.21, train acc: 7.67e-01 - valid loss: 7.31, valid acc: 5.21e-01, valid error: 4.79e-01
epoch: 13, lr: 5.46e-05 - train loss: 3.07, train acc: 7.75e-01 - valid loss: 8.11, valid acc: 4.68e-01, valid error: 5.32e-01
epoch: 14, lr: 5.88e-05 - train loss: 2.93, train acc: 7.88e-01 - valid loss: 8.76, valid acc: 4.37e-01, valid error: 5.64e-01
epoch: 15, lr: 6.30e-05 - train loss: 2.83, train acc: 7.95e-01 - valid loss: 7.76, valid acc: 5.05e-01, valid error: 4.95e-01
epoch: 16, lr: 6.72e-05 - train loss: 2.70, train acc: 8.03e-01 - valid loss: 8.43, valid acc: 4.73e-01, valid error: 5.27e-01
epoch: 17, lr: 7.14e-05 - train loss: 2.66, train acc: 8.05e-01 - valid loss: 8.82, valid acc: 4.37e-01, valid error: 5.64e-01
epoch: 18, lr: 7.56e-05 - train loss: 2.50, train acc: 8.18e-01 - valid loss: 7.78, valid acc: 4.82e-01, valid error: 5.18e-01
epoch: 19, lr: 7.98e-05 - train loss: 2.51, train acc: 8.16e-01 - valid loss: 8.42, valid acc: 4.59e-01, valid error: 5.41e-01
epoch: 20, lr: 8.40e-05 - train loss: 2.39, train acc: 8.22e-01 - valid loss: 8.10, valid acc: 4.94e-01, valid error: 5.06e-01
epoch: 21, lr: 8.82e-05 - train loss: 2.32, train acc: 8.31e-01 - valid loss: 8.34, valid acc: 4.83e-01, valid error: 5.17e-01
epoch: 22, lr: 9.24e-05 - train loss: 2.24, train acc: 8.40e-01 - valid loss: 8.41, valid acc: 4.77e-01, valid error: 5.23e-01
epoch: 23, lr: 9.66e-05 - train loss: 2.17, train acc: 8.40e-01 - valid loss: 8.29, valid acc: 4.76e-01, valid error: 5.24e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.12, train acc: 8.49e-01 - valid loss: 7.91, valid acc: 4.77e-01, valid error: 5.23e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.09, train acc: 8.44e-01 - valid loss: 8.02, valid acc: 5.06e-01, valid error: 4.94e-01
Epoch loaded: 12, 
 Per Class Accuracy: 
0: 0.690
1: 0.670
2: 0.339
3: 0.450
4: 0.410
5: 0.474
6: 0.741
7: 0.224
8: 0.500
9: 0.490, 
 Confusion Matrix: 
[[ 69  11   2   2   5   1   3   0   2   5]
 [  2  67   1   2  14   1   9   0   2   2]
 [  2   5  20   9   5   2   4   0   8   4]
 [  3   1   0  45   7   0   5  12  14  13]
 [  3  11   2  13  41   2  12   1   9   6]
 [  1   1   9   0   0  18   3   0   6   0]
 [  4   0  13   1  13   1 123   2   2   7]
 [  2   0   1  18   0   0   5  24  47  10]
 [  0   1  18   8   5   1   7   7  60  13]
 [  3   2   8  10   3   0   6   7  12  49]]
 - test loss: 7.31, test acc: 5.21e-01, test error: 4.79e-01
