epoch: 1, lr: 4.21e-06 - train loss: 7.70, train acc: 3.38e-01 - valid loss: 6.56, valid acc: 4.59e-01, valid error: 5.42e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.40, train acc: 6.99e-01 - valid loss: 5.12, valid acc: 6.37e-01, valid error: 3.70e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.51, train acc: 8.41e-01 - valid loss: 5.21, valid acc: 6.24e-01, valid error: 3.88e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.29, train acc: 9.30e-01 - valid loss: 6.10, valid acc: 5.27e-01, valid error: 4.78e-01
epoch: 5, lr: 2.11e-05 - train loss: 6.89e-01, train acc: 9.64e-01 - valid loss: 4.63, valid acc: 7.00e-01, valid error: 3.10e-01
epoch: 6, lr: 2.53e-05 - train loss: 5.17e-01, train acc: 9.74e-01 - valid loss: 5.12, valid acc: 6.68e-01, valid error: 3.42e-01
epoch: 7, lr: 2.95e-05 - train loss: 4.95e-01, train acc: 9.73e-01 - valid loss: 6.43, valid acc: 6.06e-01, valid error: 3.96e-01
epoch: 8, lr: 3.37e-05 - train loss: 4.67e-01, train acc: 9.73e-01 - valid loss: 6.04, valid acc: 6.22e-01, valid error: 3.85e-01
epoch: 9, lr: 3.79e-05 - train loss: 4.08e-01, train acc: 9.77e-01 - valid loss: 4.91, valid acc: 7.06e-01, valid error: 3.04e-01
epoch: 10, lr: 4.21e-05 - train loss: 4.37e-01, train acc: 9.73e-01 - valid loss: 7.53, valid acc: 5.65e-01, valid error: 4.38e-01
epoch: 11, lr: 4.64e-05 - train loss: 3.93e-01, train acc: 9.75e-01 - valid loss: 7.02, valid acc: 6.19e-01, valid error: 3.88e-01
epoch: 12, lr: 5.06e-05 - train loss: 3.70e-01, train acc: 9.78e-01 - valid loss: 7.16, valid acc: 6.31e-01, valid error: 3.70e-01
epoch: 13, lr: 5.48e-05 - train loss: 4.01e-01, train acc: 9.75e-01 - valid loss: 6.77, valid acc: 6.41e-01, valid error: 3.60e-01
epoch: 14, lr: 5.90e-05 - train loss: 3.44e-01, train acc: 9.78e-01 - valid loss: 5.74, valid acc: 7.09e-01, valid error: 3.00e-01
epoch: 15, lr: 6.32e-05 - train loss: 3.83e-01, train acc: 9.75e-01 - valid loss: 6.07, valid acc: 6.84e-01, valid error: 3.26e-01
epoch: 16, lr: 6.74e-05 - train loss: 3.04e-01, train acc: 9.81e-01 - valid loss: 7.02, valid acc: 6.53e-01, valid error: 3.59e-01
epoch: 17, lr: 7.17e-05 - train loss: 3.37e-01, train acc: 9.78e-01 - valid loss: 6.07, valid acc: 6.89e-01, valid error: 3.21e-01
epoch: 18, lr: 7.59e-05 - train loss: 3.36e-01, train acc: 9.79e-01 - valid loss: 6.42, valid acc: 6.52e-01, valid error: 3.49e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.53e-01, train acc: 9.77e-01 - valid loss: 7.92, valid acc: 6.16e-01, valid error: 3.86e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.20e-01, train acc: 9.78e-01 - valid loss: 5.65, valid acc: 6.73e-01, valid error: 3.33e-01
epoch: 21, lr: 8.85e-05 - train loss: 3.20e-01, train acc: 9.80e-01 - valid loss: 5.95, valid acc: 6.95e-01, valid error: 3.15e-01
epoch: 22, lr: 9.27e-05 - train loss: 2.73e-01, train acc: 9.82e-01 - valid loss: 6.51, valid acc: 7.02e-01, valid error: 3.08e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.05e-01, train acc: 9.80e-01 - valid loss: 6.26, valid acc: 7.04e-01, valid error: 2.95e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.64e-01, train acc: 9.84e-01 - valid loss: 6.90, valid acc: 6.58e-01, valid error: 3.47e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.59e-01, train acc: 9.84e-01 - valid loss: 6.01, valid acc: 7.06e-01, valid error: 2.98e-01
Epoch loaded: 23, 
 Per Class Accuracy: 
0: 0.820
1: 0.560
2: 0.867
3: 0.250
4: 0.930
5: 1.000
6: 0.863
7: 0.739
8: 0.423
9: 0.890, 
 Confusion Matrix: 
[[82  4  2  2  1  0  8  0  0  1]
 [ 0 56  2  1 19  0  6 11  1  4]
 [ 1  0 26  0  1  0  0  0  0  2]
 [ 0  4  4 25 27  0  6 11  0 23]
 [ 0  2  0  1 93  0  4  0  0  0]
 [ 0  0  0  0  0 30  0  0  0  0]
 [ 0  0  0  1  7  0 69  0  0  3]
 [ 2  0  0  2  4  0  1 65  4 10]
 [ 1  0  3 21  0  0  0  3 33 17]
 [ 1  0  0  1  4  0  3  0  2 89]]
 - test loss: 6.26, test acc: 7.04e-01, test error: 2.95e-01
