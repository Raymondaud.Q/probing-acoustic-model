epoch: 1, lr: 4.21e-06 - train loss: 7.77, train acc: 3.27e-01 - valid loss: 6.90, valid acc: 4.25e-01, valid error: 5.80e-01
epoch: 2, lr: 8.43e-06 - train loss: 4.49, train acc: 6.87e-01 - valid loss: 4.80, valid acc: 6.35e-01, valid error: 3.68e-01
epoch: 3, lr: 1.26e-05 - train loss: 2.53, train acc: 8.46e-01 - valid loss: 4.40, valid acc: 6.72e-01, valid error: 3.30e-01
epoch: 4, lr: 1.69e-05 - train loss: 1.25, train acc: 9.32e-01 - valid loss: 4.40, valid acc: 6.78e-01, valid error: 3.27e-01
epoch: 5, lr: 2.11e-05 - train loss: 6.74e-01, train acc: 9.67e-01 - valid loss: 4.66, valid acc: 6.86e-01, valid error: 3.09e-01
epoch: 6, lr: 2.53e-05 - train loss: 5.42e-01, train acc: 9.73e-01 - valid loss: 4.84, valid acc: 6.85e-01, valid error: 3.16e-01
epoch: 7, lr: 2.95e-05 - train loss: 4.58e-01, train acc: 9.74e-01 - valid loss: 6.53, valid acc: 5.57e-01, valid error: 4.44e-01
epoch: 8, lr: 3.37e-05 - train loss: 4.17e-01, train acc: 9.76e-01 - valid loss: 4.76, valid acc: 6.97e-01, valid error: 3.08e-01
epoch: 9, lr: 3.79e-05 - train loss: 4.05e-01, train acc: 9.77e-01 - valid loss: 4.69, valid acc: 7.09e-01, valid error: 2.92e-01
epoch: 10, lr: 4.21e-05 - train loss: 3.90e-01, train acc: 9.78e-01 - valid loss: 4.92, valid acc: 7.05e-01, valid error: 2.99e-01
epoch: 11, lr: 4.64e-05 - train loss: 4.04e-01, train acc: 9.76e-01 - valid loss: 4.90, valid acc: 7.16e-01, valid error: 2.82e-01
epoch: 12, lr: 5.06e-05 - train loss: 3.57e-01, train acc: 9.78e-01 - valid loss: 4.83, valid acc: 7.16e-01, valid error: 2.88e-01
epoch: 13, lr: 5.48e-05 - train loss: 3.97e-01, train acc: 9.75e-01 - valid loss: 5.19, valid acc: 6.86e-01, valid error: 3.15e-01
epoch: 14, lr: 5.90e-05 - train loss: 3.69e-01, train acc: 9.77e-01 - valid loss: 4.75, valid acc: 7.48e-01, valid error: 2.52e-01
epoch: 15, lr: 6.32e-05 - train loss: 3.32e-01, train acc: 9.79e-01 - valid loss: 5.01, valid acc: 7.24e-01, valid error: 2.77e-01
epoch: 16, lr: 6.74e-05 - train loss: 4.05e-01, train acc: 9.74e-01 - valid loss: 5.58, valid acc: 6.99e-01, valid error: 2.96e-01
epoch: 17, lr: 7.17e-05 - train loss: 3.42e-01, train acc: 9.79e-01 - valid loss: 7.98, valid acc: 5.44e-01, valid error: 4.62e-01
epoch: 18, lr: 7.59e-05 - train loss: 3.39e-01, train acc: 9.79e-01 - valid loss: 5.32, valid acc: 7.05e-01, valid error: 2.99e-01
epoch: 19, lr: 8.01e-05 - train loss: 3.28e-01, train acc: 9.80e-01 - valid loss: 5.44, valid acc: 6.77e-01, valid error: 3.28e-01
epoch: 20, lr: 8.43e-05 - train loss: 3.73e-01, train acc: 9.76e-01 - valid loss: 5.12, valid acc: 6.72e-01, valid error: 3.30e-01
epoch: 21, lr: 8.85e-05 - train loss: 2.96e-01, train acc: 9.81e-01 - valid loss: 5.03, valid acc: 6.88e-01, valid error: 3.17e-01
epoch: 22, lr: 9.27e-05 - train loss: 3.02e-01, train acc: 9.79e-01 - valid loss: 5.49, valid acc: 6.99e-01, valid error: 3.02e-01
epoch: 23, lr: 9.69e-05 - train loss: 3.13e-01, train acc: 9.81e-01 - valid loss: 6.17, valid acc: 6.70e-01, valid error: 3.35e-01
epoch: 24, lr: 1.01e-04 - train loss: 2.61e-01, train acc: 9.84e-01 - valid loss: 6.06, valid acc: 7.04e-01, valid error: 2.91e-01
epoch: 25, lr: 1.05e-04 - train loss: 2.76e-01, train acc: 9.82e-01 - valid loss: 5.38, valid acc: 6.93e-01, valid error: 3.09e-01
Epoch loaded: 14, 
 Per Class Accuracy: 
0: 0.633
1: 0.850
2: 0.630
3: 0.920
4: 0.810
5: 0.950
6: 0.915
7: 0.308
8: 1.000
9: 0.610, 
 Confusion Matrix: 
[[ 62   0   3   3   6   1  14   5   0   4]
 [  0  85   1   2   1   1   5   4   1   0]
 [  0   1  63   0  14   0   8   4   1   9]
 [  0   0   0  92   2   0   0   6   0   0]
 [  0   5   2   2  81   0   6   0   1   3]
 [  0   2   0   0   0  38   0   0   0   0]
 [  0   1   1   1   3   0  65   0   0   0]
 [  1   0   4   7   8   0   0  33  25  29]
 [  0   0   0   0   0   0   0   0 120   0]
 [  1   2   1  10   3   1   4   7  10  61]]
 - test loss: 4.75, test acc: 7.48e-01, test error: 2.52e-01
