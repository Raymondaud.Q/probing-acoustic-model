epoch: 1, lr: 4.21e-06 - train loss: 7.44, train acc: 3.56e-01 - valid loss: 6.24, valid acc: 5.47e-01, valid error: 4.48e-01
epoch: 2, lr: 8.43e-06 - train loss: 5.14, train acc: 6.17e-01 - valid loss: 5.46, valid acc: 6.59e-01, valid error: 3.39e-01
epoch: 3, lr: 1.26e-05 - train loss: 4.06, train acc: 7.18e-01 - valid loss: 5.68, valid acc: 6.50e-01, valid error: 3.49e-01
epoch: 4, lr: 1.69e-05 - train loss: 3.40, train acc: 7.68e-01 - valid loss: 5.35, valid acc: 6.28e-01, valid error: 3.72e-01
epoch: 5, lr: 2.11e-05 - train loss: 2.76, train acc: 8.13e-01 - valid loss: 5.42, valid acc: 6.14e-01, valid error: 3.79e-01
epoch: 6, lr: 2.53e-05 - train loss: 2.16, train acc: 8.62e-01 - valid loss: 5.13, valid acc: 6.63e-01, valid error: 3.29e-01
epoch: 7, lr: 2.95e-05 - train loss: 1.84, train acc: 8.83e-01 - valid loss: 4.77, valid acc: 6.94e-01, valid error: 2.96e-01
epoch: 8, lr: 3.37e-05 - train loss: 1.55, train acc: 9.04e-01 - valid loss: 6.00, valid acc: 6.48e-01, valid error: 3.44e-01
epoch: 9, lr: 3.79e-05 - train loss: 1.36, train acc: 9.12e-01 - valid loss: 6.60, valid acc: 5.97e-01, valid error: 4.03e-01
epoch: 10, lr: 4.21e-05 - train loss: 1.20, train acc: 9.23e-01 - valid loss: 5.95, valid acc: 6.34e-01, valid error: 3.58e-01
epoch: 11, lr: 4.64e-05 - train loss: 1.18, train acc: 9.25e-01 - valid loss: 5.07, valid acc: 7.00e-01, valid error: 3.03e-01
epoch: 12, lr: 5.06e-05 - train loss: 9.09e-01, train acc: 9.41e-01 - valid loss: 5.71, valid acc: 6.58e-01, valid error: 3.33e-01
epoch: 13, lr: 5.48e-05 - train loss: 9.78e-01, train acc: 9.37e-01 - valid loss: 5.44, valid acc: 6.85e-01, valid error: 3.06e-01
epoch: 14, lr: 5.90e-05 - train loss: 7.68e-01, train acc: 9.51e-01 - valid loss: 5.69, valid acc: 6.68e-01, valid error: 3.24e-01
epoch: 15, lr: 6.32e-05 - train loss: 7.46e-01, train acc: 9.53e-01 - valid loss: 5.38, valid acc: 6.91e-01, valid error: 3.06e-01
epoch: 16, lr: 6.74e-05 - train loss: 7.06e-01, train acc: 9.54e-01 - valid loss: 5.91, valid acc: 6.79e-01, valid error: 3.12e-01
epoch: 17, lr: 7.17e-05 - train loss: 6.43e-01, train acc: 9.58e-01 - valid loss: 5.43, valid acc: 6.97e-01, valid error: 2.94e-01
epoch: 18, lr: 7.59e-05 - train loss: 6.20e-01, train acc: 9.61e-01 - valid loss: 5.50, valid acc: 6.87e-01, valid error: 3.03e-01
epoch: 19, lr: 8.01e-05 - train loss: 5.16e-01, train acc: 9.68e-01 - valid loss: 5.28, valid acc: 6.94e-01, valid error: 3.03e-01
epoch: 20, lr: 8.43e-05 - train loss: 5.16e-01, train acc: 9.66e-01 - valid loss: 5.65, valid acc: 6.98e-01, valid error: 2.93e-01
epoch: 21, lr: 8.85e-05 - train loss: 4.73e-01, train acc: 9.70e-01 - valid loss: 6.26, valid acc: 6.48e-01, valid error: 3.44e-01
epoch: 22, lr: 9.27e-05 - train loss: 4.83e-01, train acc: 9.68e-01 - valid loss: 5.38, valid acc: 7.06e-01, valid error: 2.84e-01
epoch: 23, lr: 9.69e-05 - train loss: 4.13e-01, train acc: 9.74e-01 - valid loss: 5.17, valid acc: 7.18e-01, valid error: 2.78e-01
epoch: 24, lr: 1.01e-04 - train loss: 4.23e-01, train acc: 9.72e-01 - valid loss: 5.37, valid acc: 7.13e-01, valid error: 2.83e-01
epoch: 25, lr: 1.05e-04 - train loss: 4.51e-01, train acc: 9.70e-01 - valid loss: 5.57, valid acc: 7.03e-01, valid error: 2.87e-01
Epoch loaded: 23, 
 Per Class Accuracy: 
0: 0.760
1: 0.690
2: 0.880
3: 0.820
4: 0.812
5: 0.578
6: 0.688
7: 0.802
8: 0.440
9: 0.909, 
 Confusion Matrix: 
[[76  3  1  1  1  8  3  1  4  2]
 [ 6 69  8 13  0  0  4  0  0  0]
 [ 2  1 88  0  0  1  7  0  0  1]
 [ 3  9  1 82  0  0  0  0  5  0]
 [ 0  0  0  0 26  1  0  0  4  1]
 [22  5  2  0  0 48  0  0  6  0]
 [ 0  4 23  1  0  0 64  1  0  0]
 [ 0  0  0  1  2  0 15 77  1  0]
 [ 6  1 19  8  2 14  3  2 44  1]
 [ 1  0  0  2  0  0  0  0  0 30]]
 - test loss: 5.17, test acc: 7.18e-01, test error: 2.78e-01
