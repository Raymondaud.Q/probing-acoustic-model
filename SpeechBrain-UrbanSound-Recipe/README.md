
# UrbanSound Results : 10 Fold X-Validation Acoustic Environments 

[Results on 10-Fold X-Validation Experiments](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/-/tree/master/SpeechBrain-UrbanSound-Recipe/10_Fold) 

# UrbanSound - Kaldi Recipe

[Tools](https://gitlab.com/Raymondaud.Q/probing-acoustic-model/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts) every script you need to reproduce our exps.
