#!/bin/sh

#SBATCH --ntasks=1
#SBATCH --partition=gpu
#SBATCH --cpus-per-task=16
#SBATCH --gpus-per-node=1
#SBATCH --mem=50G
#SBATCH --time=20-00:00:00
#SBATCH --mail-type=ALL
#SBATCH --exclude=eris,calypso,alpos,talos

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[2, 3, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[1] --test_fold_nums=[1] --output_folder=./results/kaldi/$1/fold_1 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 3, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[2] --test_fold_nums=[2] --output_folder=./results/kaldi/$1/fold_2 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 4, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[3] --test_fold_nums=[3] --output_folder=./results/kaldi/$1/fold_3 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 5, 6, 7, 8, 9, 10] --valid_fold_nums=[4] --test_fold_nums=[4] --output_folder=./results/kaldi/$1/fold_4 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 4, 6, 7, 8, 9, 10] --valid_fold_nums=[5] --test_fold_nums=[5] --output_folder=./results/kaldi/$1/fold_5 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 4, 5, 7, 8, 9, 10] --valid_fold_nums=[6] --test_fold_nums=[6] --output_folder=./results/kaldi/$1/fold_6 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 4, 5, 6, 8, 9, 10] --valid_fold_nums=[7] --test_fold_nums=[7] --output_folder=./results/kaldi/$1/fold_7 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 9, 10] --valid_fold_nums=[8] --test_fold_nums=[8] --output_folder=./results/kaldi/$1/fold_8 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1  --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 8, 10] --valid_fold_nums=[9] --test_fold_nums=[9] --output_folder=./results/kaldi/$1/fold_9 --data_parallel_backend

python train.py hparams/train_ecapa_tdnn.yaml --model=$1 --train_fold_nums=[1, 2, 3, 4, 5, 6, 7, 8, 9] --valid_fold_nums=[10] --test_fold_nums=[10] --output_folder=./results/kaldi/$1/fold_10 --data_parallel_backend

