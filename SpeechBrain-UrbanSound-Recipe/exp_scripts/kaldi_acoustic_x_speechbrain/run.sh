#!/usr/bin/env bash
#SBATCH --job-name=extract
#SBATCH --ntasks=1
#SBATCH --partition=cpuonly
#SBATCH --cpus-per-task=2
#SBATCH --mem=2G
#SBATCH --time=4-00:00:00
#SBATCH --mail-type=ALL

. ./cmd.sh
. ./path.sh

kaldi=/data/UrbanSound8K/kaldi
models=/data/acoustic_models/

mkdir -p ${kaldi}_extract/

for model in `ls ${models}/*.raw`
do

  for fold in fold1 fold2 fold3 fold4 fold5 fold6 fold7 fold8 fold9 fold10; do
    utils/fix_data_dir.sh ${kaldi}/${fold}
    modelName=`basename ${model} .raw`
    mkdir -p ${kaldi}_extract/${modelName}
    mkdir -p ${kaldi}_extract/${modelName}/${fold}
    nnet3-compute --use-gpu=no $model scp:${kaldi}/${fold}/feats.scp ark,scp:${kaldi}_extract/${modelName}/${fold}/feats.ark,${kaldi}_extract/${modelName}/${fold}/feats.scp
    cp ${kaldi}/${fold}/utt2spk ${kaldi}_extract/${modelName}/${fold}/utt2spk
    utils/fix_data_dir.sh ${kaldi}_extract/${modelName}/${fold}
  done

done