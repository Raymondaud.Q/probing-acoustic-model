
# What ?

This folder contains Kaldi Librispeech Acoustic Model and everything to cut and use it.
Data used are UrbanSound MFCC from [Kaldi MFCC](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe) with Kaldi pybind11 branch. Installation Guide is provide in the previous link. 

We are training Speechbrain recipe with MFCC transformed by the acoustic model thanks to pybind : 

![schema](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/raw/master/Documents/Kaldi_Acoustic_Into_SpeechBrain_UrbanSound.jpg)

# Preparation

[Follow Kaldi MFCC's ReadMe](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe)
Paste the content of this directory in a copie of `/speechbrain/recipes/UrbanSound8k/SoundClassification` 

* Execute :
	* `. path.sh`
	* `preparestuff.sh` or use `acoustic_models.zip`
		* It splits acoustic model copy on Affine 1 f4 f8 f12 f16 layers.  
	
* Run `run.sh` :
	* Transforms [Kaldi MFCC](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/tree/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/kaldi_mfcc_recipe) using acoustic model splits.  

* Run `slurm_train.sh` :  
	* `sbatch -J <model> slurm_train.sh <model>` with `<model>` = final1, ..., finalf16 .
		* Allows to run UrbanSound8K recipe with features extracted at a specific depth

