#!/usr/bin/env bash

. ./cmd.sh
. ./path.sh
mfccdir=`pwd`/mfcc
audio=/data/coros1/qraymondaud/UrbanSound8K/audio
kaldi=/data/coros1/qraymondaud/UrbanSound8K/kaldi

echo $1

rm -rf $kaldi/$1/
mkdir -p $kaldi/$1/

touch $kaldi/$1/wav.scp
touch $kaldi/$1/utt2spk
touch $kaldi/$1/spk2utt

for file in `ls $audio/$1/*.wav`
do
  fbasename=`basename $file .wav`
  echo "${fbasename} sox ${audio}/$1/${fbasename}.wav -r 16000 -c 1 -b 16 -t wav - |" >> $kaldi/$1/wav.scp
  echo "${fbasename} ${fbasename}" >> $kaldi/$1/utt2spk
  echo "${fbasename} ${fbasename}" >> $kaldi/$1/spk2utt
done

utils/fix_data_dir.sh $kaldi/$1/
steps/make_mfcc.sh --mfcc-config conf/mfcc_hires.conf --nj 20 --cmd "$train_cmd" $kaldi/$1/
# Verif :  copy-feats scp:/data/coros1/qraymondaud/UrbanSound8K/kaldi/fold1/data/raw_mfcc_fold1.1.scp ark,t:- | more

