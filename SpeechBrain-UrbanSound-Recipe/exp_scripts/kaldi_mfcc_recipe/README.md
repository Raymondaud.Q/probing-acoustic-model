# Install Kaldi X SpeechBrain

* Install [KALDI pybind11 branch](https://github.com/kaldi-asr/kaldi/tree/pybind11)

	* Unmaintained branch, many problems :

		* Put [archive src.zip ](https://gitlab.com/Raymondaud.Q/lia-alternance-m2/-/blob/master/SpeechBrain-UrbanSound-Recipe/exp_scripts/src.zip) in kaldi/src

		* Follow [Kaldi Installation Guide from pybind11 branch](https://github.com/kaldi-asr/kaldi/blob/pybind11/src/INSTALL)

			* ctc/warp-ctc/build/libwarpctc.so removal in make 
			* ctc/warp-ctc/build/libwarpctc.so removal in the compilation command displayed in cli   
			* Dynamic install "\*.so"

		* Follow [kaldi/pybind11/src/pybind Installation Guide](https://github.com/kaldi-asr/kaldi/blob/pybind11/src/pybind/README.md)

		* Continue to fix it until it works :)

	* cp -r kaldi/egs/librispeech/ kaldi/egs/urbansound

* **Put the two listed files in** `kaldi/egs/urbansound/s5` 

	* ./kaldi_mfcc_recipe/run-bis.sh (check UrbanSound8k dataset path)

	* ./kaldi_mfcc_recipe/slurm_run_bis.sh

	* **Configure Kaldi in PATH variable** in order to use pybind in any project `speechbrain/recipes/UrbanSound8k/SoundClassification` (See ./kaldi_mfcc_recipe/path.sh and ./kaldi_mfcc_recipe/cmd.sh)

	* **You can also append pybind to PATH directly in python** by using `sys.path.append(`**\<KALDI_ROOT\>/kaldi/src/pybind**`)`


* **Put below files in** `speechbrain/recipes/UrbanSound8k/SoundClassification`

	* ./kaldi_mfcc_recipe/train.py
	* ./kaldi_mfcc_recipe/slurm_train.sh
	* ./kaldi_mfcc_recipe/hparams/* (Check datapath in yaml)

# Run UrbanSound8K Kaldi - Speechbrain

1. Run `slurm_run_bis.sh` for each of the 10 folds

	* IE: cd `kaldi/egs/urbansound/s5` : 

		* ./slurm_run_bis.sh fold1 
		* ./slurm_run_bis.sh fold2
		* etc 

2. Run `slurm_train.sh` from `speechbrain/recipes/UrbanSound8k/SoundClassification`