#!/bin/bash

#SBATCH --job-name=US_AUG
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpu
#SBATCH --gpus-per-node=4
#SBATCH --mem=8G
#SBATCH --time=2-00:00:00
#SBATCH --mail-type=ALL
#SBATCH --exclude=idyie

python train.py hparams/train_ecapa_tdnn.yaml --output_folder=./results/urban_sound/with_aug --data_parallel_backend
